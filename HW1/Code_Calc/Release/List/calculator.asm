
;CodeVisionAVR C Compiler V3.12 Advanced
;(C) Copyright 1998-2014 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Build configuration    : Release
;Chip type              : ATmega16
;Program type           : Application
;Clock frequency        : 4.000000 MHz
;Memory model           : Small
;Optimize for           : Size
;(s)printf features     : int, width
;(s)scanf features      : int, width
;External RAM size      : 0
;Data Stack size        : 256 byte(s)
;Heap size              : 0 byte(s)
;Promote 'char' to 'int': Yes
;'char' is unsigned     : Yes
;8 bit enums            : Yes
;Global 'const' stored in FLASH: Yes
;Enhanced function parameter passing: Yes
;Enhanced core instructions: On
;Automatic register allocation for global variables: On
;Smart register allocation: On

	#define _MODEL_SMALL_

	#pragma AVRPART ADMIN PART_NAME ATmega16
	#pragma AVRPART MEMORY PROG_FLASH 16384
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1024
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0060
	.EQU __SRAM_END=0x045F
	.EQU __DSTACK_SIZE=0x0100
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTW2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	LDI  R24,BYTE3(2*@0+(@1))
	LDI  R25,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	PUSH R26
	PUSH R27
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	POP  R27
	POP  R26
	ICALL
	.ENDM

	.MACRO __CALL2EX
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	CALL __EEPROMRDD
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R30,SPL
	IN   R31,SPH
	ADIW R30,@0+1
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1STACK
	IN   R30,SPL
	IN   R31,SPH
	ADIW R30,@0+1
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z
	MOVW R30,R0
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _state=R4
	.DEF _state_msb=R5
	.DEF _num=R6
	.DEF _num_msb=R7
	.DEF _exp_len=R8
	.DEF _exp_len_msb=R9
	.DEF _num_flag=R10
	.DEF _num_flag_msb=R11
	.DEF __lcd_x=R13
	.DEF __lcd_y=R12

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00

_shift:
	.DB  0x77,0x6F,0x5F,0x3F
_layout:
	.DB  0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38
	.DB  0x39,0x2A,0x30,0x23
_tbl10_G101:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G101:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

;GLOBAL REGISTER VARIABLES INITIALIZATION
__REG_VARS:
	.DB  0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0

_0x0:
	.DB  0x30,0x3A,0x4E,0x75,0x6D,0x20,0x20,0x31
	.DB  0x3A,0x42,0x61,0x73,0x69,0x63,0x20,0x20
	.DB  0x32,0x3A,0x41,0x64,0x76,0x20,0x20,0x33
	.DB  0x2E,0x47,0x65,0x6F,0x0,0x45,0x78,0x70
	.DB  0x72,0x3A,0x20,0x0,0x45,0x72,0x72,0x6F
	.DB  0x72,0x0,0x45,0x6E,0x74,0x65,0x72,0x20
	.DB  0x2A,0x20,0x74,0x6F,0x20,0x63,0x6C,0x65
	.DB  0x61,0x72,0x2E,0x0,0x25,0x73,0x3D,0x25
	.DB  0x73,0x0,0x45,0x6E,0x74,0x65,0x72,0x20
	.DB  0x41,0x20,0x44,0x69,0x67,0x69,0x74,0x3A
	.DB  0x20,0x30,0x2D,0x39,0x0,0x31,0x3A,0x2B
	.DB  0x20,0x20,0x32,0x3A,0x2D,0x20,0x20,0x33
	.DB  0x3A,0x2F,0x20,0x20,0x34,0x3A,0x2A,0x20
	.DB  0x20,0x35,0x3A,0x5E,0x20,0x36,0x3A,0x28
	.DB  0x20,0x37,0x3A,0x29,0x0,0x31,0x3A,0x73
	.DB  0x71,0x72,0x74,0x20,0x20,0x32,0x3A,0x65
	.DB  0x78,0x70,0x20,0x20,0x33,0x3A,0x6C,0x6F
	.DB  0x67,0x20,0x20,0x34,0x3A,0x6E,0x65,0x67
	.DB  0x0,0x31,0x3A,0x73,0x69,0x6E,0x20,0x20
	.DB  0x32,0x3A,0x63,0x6F,0x73,0x20,0x20,0x33
	.DB  0x3A,0x74,0x61,0x6E,0x0,0x25,0x73,0x25
	.DB  0x63,0x0,0x25,0x73,0x73,0x71,0x72,0x74
	.DB  0x0,0x25,0x73,0x65,0x78,0x70,0x0,0x25
	.DB  0x73,0x6C,0x6F,0x67,0x0,0x25,0x73,0x6E
	.DB  0x65,0x67,0x0,0x25,0x73,0x2B,0x0,0x25
	.DB  0x73,0x2D,0x0,0x25,0x73,0x2F,0x0,0x25
	.DB  0x73,0x2A,0x0,0x25,0x73,0x5E,0x0,0x25
	.DB  0x73,0x28,0x0,0x25,0x73,0x29,0x0,0x25
	.DB  0x73,0x73,0x69,0x6E,0x0,0x25,0x73,0x63
	.DB  0x6F,0x73,0x0,0x25,0x73,0x74,0x61,0x6E
	.DB  0x0
_0x2000003:
	.DB  0x80,0xC0
_0x2060060:
	.DB  0x1
_0x2060000:
	.DB  0x2D,0x4E,0x41,0x4E,0x0,0x49,0x4E,0x46
	.DB  0x0

__GLOBAL_INI_TBL:
	.DW  0x08
	.DW  0x04
	.DW  __REG_VARS*2

	.DW  0x02
	.DW  __base_y_G100
	.DW  _0x2000003*2

	.DW  0x01
	.DW  __seed_G103
	.DW  _0x2060060*2

_0xFFFFFFFF:
	.DW  0

#define __GLOBAL_INI_TBL_PRESENT 1

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,__SRAM_START
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x160

	.CSEG
;#include <mega16.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x40
	.EQU __sm_mask=0xB0
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0xA0
	.EQU __sm_ext_standby=0xB0
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <alcd.h>
;#include <delay.h>
;#include <stdio.h>
;#include <string.h>
;#include <stdlib.h>
;#include <math.h>
;
;// states
;#define DEFAULT 0
;#define NUMBER 1
;#define ADVANCED 2
;#define GEO 5
;#define PRIMARY 3
;#define RESULT 4
;
;#define ERROR 123456789
;
;char keypad(void);
;void fsm(void);
;float evaluate(void);
;int get_last_num(int);
;int get_next_num(int, int);
;
;
;flash char shift[4] = {0x77, 0x6F, 0x5F, 0x3F};
;flash char layout[12] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#'};
;char msg[32];
;char expression[32];
;float val[32];
;char types[32];
;int state = DEFAULT;
;int num;
;int exp_len;
;int num_flag = 0;
;
;
;void main(void)
; 0000 0027 {

	.CSEG
_main:
; .FSTART _main
; 0000 0028     int i;
; 0000 0029     PORTD = 0x7F;
;	i -> R16,R17
	LDI  R30,LOW(127)
	OUT  0x12,R30
; 0000 002A     DDRD = 0x78;
	LDI  R30,LOW(120)
	OUT  0x11,R30
; 0000 002B     lcd_init(32);
	LDI  R26,LOW(32)
	CALL _lcd_init
; 0000 002C     sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
	CALL SUBOPT_0x0
	CALL SUBOPT_0x1
; 0000 002D     exp_len = 0;
	CLR  R8
	CLR  R9
; 0000 002E     sprintf(expression, "Expr: ");
	CALL SUBOPT_0x2
	CALL SUBOPT_0x3
; 0000 002F     for (i = 0; i < 32; i ++){
	__GETWRN 16,17,0
_0x4:
	__CPWRN 16,17,32
	BRGE _0x5
; 0000 0030         val[i] = 0.0;
	CALL SUBOPT_0x4
	CALL SUBOPT_0x5
; 0000 0031         types[i] = 'x';
	CALL SUBOPT_0x6
	LDI  R30,LOW(120)
	ST   X,R30
; 0000 0032     }
	__ADDWRN 16,17,1
	RJMP _0x4
_0x5:
; 0000 0033 
; 0000 0034     while (1)
_0x6:
; 0000 0035     {
; 0000 0036         lcd_clear();
	CALL _lcd_clear
; 0000 0037         lcd_gotoxy(0, 0);
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _lcd_gotoxy
; 0000 0038         lcd_puts(msg);
	LDI  R26,LOW(_msg)
	LDI  R27,HIGH(_msg)
	CALL _lcd_puts
; 0000 0039         lcd_gotoxy(0, 1);
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _lcd_gotoxy
; 0000 003A         lcd_puts(expression);
	LDI  R26,LOW(_expression)
	LDI  R27,HIGH(_expression)
	CALL _lcd_puts
; 0000 003B         fsm();
	RCALL _fsm
; 0000 003C     }
	RJMP _0x6
; 0000 003D }
_0x9:
	RJMP _0x9
; .FEND
;
;void fsm(void)
; 0000 0040 {
_fsm:
; .FSTART _fsm
; 0000 0041     char ch = keypad();
; 0000 0042     char expr[32];
; 0000 0043     char res[10];
; 0000 0044     float result = 0.0;
; 0000 0045 
; 0000 0046     if (ch == '#'){
	SBIW R28,46
	CALL SUBOPT_0x7
	LDI  R30,LOW(0)
	STD  Y+2,R30
	STD  Y+3,R30
	ST   -Y,R17
;	ch -> R17
;	expr -> Y+15
;	res -> Y+5
;	result -> Y+1
	RCALL _keypad
	MOV  R17,R30
	CPI  R17,35
	BREQ PC+2
	RJMP _0xA
; 0000 0047         if (num_flag == 1){
	CALL SUBOPT_0x8
	BRNE _0xB
; 0000 0048             val[exp_len] = num;
	CALL SUBOPT_0x9
; 0000 0049             num_flag = 0;
	CALL SUBOPT_0xA
; 0000 004A             types[exp_len] = 'N';
; 0000 004B             exp_len ++;
; 0000 004C             num = 0;
; 0000 004D         }
; 0000 004E         state = RESULT;
_0xB:
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	MOVW R4,R30
; 0000 004F         result = evaluate();
	RCALL _evaluate
	__PUTD1S 1
; 0000 0050         if (result == ERROR){
	CALL SUBOPT_0xB
	__CPD2N 0x4CEB79A3
	BRNE _0xC
; 0000 0051             sprintf(res, "Error");
	MOVW R30,R28
	ADIW R30,5
	ST   -Y,R31
	ST   -Y,R30
	__POINTW1FN _0x0,36
	CALL SUBOPT_0xC
; 0000 0052         } else{
	RJMP _0xD
_0xC:
; 0000 0053             ftoa(result, 2, res);
	CALL SUBOPT_0xD
	CALL __PUTPARD1
	LDI  R30,LOW(2)
	ST   -Y,R30
	MOVW R26,R28
	ADIW R26,10
	CALL _ftoa
; 0000 0054         }
_0xD:
; 0000 0055         sprintf(msg, "Enter * to clear.");
	CALL SUBOPT_0x0
	__POINTW1FN _0x0,42
	CALL SUBOPT_0xC
; 0000 0056         sprintf(expr, "%s=%s", expression, res);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,60
	CALL SUBOPT_0xF
	MOVW R30,R28
	ADIW R30,13
	CLR  R22
	CLR  R23
	CALL SUBOPT_0x10
; 0000 0057         strcpy(expression, expr);
	CALL SUBOPT_0x11
; 0000 0058         return;
	RJMP _0x20C000F
; 0000 0059     }
; 0000 005A 
; 0000 005B     switch(state){
_0xA:
	MOVW R30,R4
; 0000 005C         case DEFAULT:
	SBIW R30,0
	BRNE _0x11
; 0000 005D             switch(ch){
	MOV  R30,R17
	LDI  R31,0
; 0000 005E                 case '0':
	CPI  R30,LOW(0x30)
	LDI  R26,HIGH(0x30)
	CPC  R31,R26
	BRNE _0x15
; 0000 005F                     state = NUMBER;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	MOVW R4,R30
; 0000 0060                     sprintf(msg, "Enter A Digit: 0-9");
	CALL SUBOPT_0x0
	__POINTW1FN _0x0,66
	RJMP _0xA1
; 0000 0061                     break;
; 0000 0062                 case '1':
_0x15:
	CPI  R30,LOW(0x31)
	LDI  R26,HIGH(0x31)
	CPC  R31,R26
	BRNE _0x16
; 0000 0063                     state = PRIMARY;
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	MOVW R4,R30
; 0000 0064                     sprintf(msg, "1:+  2:-  3:/  4:*  5:^ 6:( 7:)");
	CALL SUBOPT_0x0
	__POINTW1FN _0x0,85
	RJMP _0xA1
; 0000 0065                     break;
; 0000 0066                 case '2':
_0x16:
	CPI  R30,LOW(0x32)
	LDI  R26,HIGH(0x32)
	CPC  R31,R26
	BRNE _0x17
; 0000 0067                     state = ADVANCED;
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	MOVW R4,R30
; 0000 0068                     sprintf(msg, "1:sqrt  2:exp  3:log  4:neg");
	CALL SUBOPT_0x0
	__POINTW1FN _0x0,117
	RJMP _0xA1
; 0000 0069                     break;
; 0000 006A                 case '3':
_0x17:
	CPI  R30,LOW(0x33)
	LDI  R26,HIGH(0x33)
	CPC  R31,R26
	BRNE _0x14
; 0000 006B                     state = GEO;
	LDI  R30,LOW(5)
	LDI  R31,HIGH(5)
	MOVW R4,R30
; 0000 006C                     sprintf(msg, "1:sin  2:cos  3:tan");
	CALL SUBOPT_0x0
	__POINTW1FN _0x0,145
_0xA1:
	ST   -Y,R31
	ST   -Y,R30
	LDI  R24,0
	CALL _sprintf
	ADIW R28,4
; 0000 006D                     break;
; 0000 006E             }
_0x14:
; 0000 006F             break;
	RJMP _0x10
; 0000 0070 
; 0000 0071         case NUMBER:
_0x11:
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	BRNE _0x19
; 0000 0072             if (ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7' ||  ...
	CPI  R17,48
	BREQ _0x1B
	CPI  R17,49
	BREQ _0x1B
	CPI  R17,50
	BREQ _0x1B
	CPI  R17,51
	BREQ _0x1B
	CPI  R17,52
	BREQ _0x1B
	CPI  R17,53
	BREQ _0x1B
	CPI  R17,54
	BREQ _0x1B
	CPI  R17,55
	BREQ _0x1B
	CPI  R17,56
	BREQ _0x1B
	CPI  R17,57
	BRNE _0x1A
_0x1B:
; 0000 0073                 num_flag = 1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	MOVW R10,R30
; 0000 0074                 num = num * 10 + (int)(ch - '0');
	MOVW R30,R6
	LDI  R26,LOW(10)
	LDI  R27,HIGH(10)
	CALL __MULW12
	MOVW R26,R30
	MOV  R30,R17
	LDI  R31,0
	SBIW R30,48
	ADD  R30,R26
	ADC  R31,R27
	MOVW R6,R30
; 0000 0075                 state =  DEFAULT;
	CALL SUBOPT_0x12
; 0000 0076                 sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
	CALL SUBOPT_0x1
; 0000 0077                 sprintf(expr, "%s%c", expression, ch);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,165
	CALL SUBOPT_0xF
	MOV  R30,R17
	CLR  R31
	CLR  R22
	CLR  R23
	CALL SUBOPT_0x10
; 0000 0078                 strcpy(expression, expr);
	CALL SUBOPT_0x11
; 0000 0079             }
; 0000 007A             break;
_0x1A:
	RJMP _0x10
; 0000 007B 
; 0000 007C         case ADVANCED:
_0x19:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	BREQ PC+2
	RJMP _0x1D
; 0000 007D             if (num_flag == 1){
	CALL SUBOPT_0x8
	BRNE _0x1E
; 0000 007E                 num_flag = 0;
	CLR  R10
	CLR  R11
; 0000 007F                 val[exp_len] = num;
	CALL SUBOPT_0x9
; 0000 0080                 types[exp_len] = 'N';
	CALL SUBOPT_0x13
	CALL SUBOPT_0x14
; 0000 0081                 exp_len ++;
; 0000 0082                 num = 0;
; 0000 0083             }
; 0000 0084 
; 0000 0085             if (ch == '1' || ch == '2' || ch == '3' || ch == '4'){
_0x1E:
	CPI  R17,49
	BREQ _0x20
	CPI  R17,50
	BREQ _0x20
	CPI  R17,51
	BREQ _0x20
	CPI  R17,52
	BRNE _0x1F
_0x20:
; 0000 0086                 state =  DEFAULT;
	CALL SUBOPT_0x12
; 0000 0087                 sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
	CALL SUBOPT_0x1
; 0000 0088                 if (ch == '1'){
	CPI  R17,49
	BRNE _0x22
; 0000 0089                     sprintf(expr, "%ssqrt", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,170
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 008A                     types[exp_len] = 'S';
	LDI  R30,LOW(83)
	ST   X,R30
; 0000 008B                 }
; 0000 008C                 if (ch == '2') {
_0x22:
	CPI  R17,50
	BRNE _0x23
; 0000 008D                     sprintf(expr, "%sexp", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,177
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 008E                     types[exp_len] = 'e';
	LDI  R30,LOW(101)
	ST   X,R30
; 0000 008F                 }
; 0000 0090                 if (ch == '3') {
_0x23:
	CPI  R17,51
	BRNE _0x24
; 0000 0091                     sprintf(expr, "%slog", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,183
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 0092                     types[exp_len] = 'l';
	LDI  R30,LOW(108)
	ST   X,R30
; 0000 0093                 }
; 0000 0094                 if (ch == '4') {
_0x24:
	CPI  R17,52
	BRNE _0x25
; 0000 0095                     sprintf(expr, "%sneg", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,189
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 0096                     types[exp_len] = 'n';
	LDI  R30,LOW(110)
	ST   X,R30
; 0000 0097                 }
; 0000 0098                 strcpy(expression, expr);
_0x25:
	CALL SUBOPT_0x2
	CALL SUBOPT_0x11
; 0000 0099                 exp_len ++;
	MOVW R30,R8
	ADIW R30,1
	MOVW R8,R30
; 0000 009A             }
; 0000 009B             break;
_0x1F:
	RJMP _0x10
; 0000 009C 
; 0000 009D         case PRIMARY:
_0x1D:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BREQ PC+2
	RJMP _0x26
; 0000 009E             if (num_flag == 1){
	CALL SUBOPT_0x8
	BRNE _0x27
; 0000 009F                 val[exp_len] = num;
	CALL SUBOPT_0x9
; 0000 00A0                 num_flag = 0;
	CALL SUBOPT_0xA
; 0000 00A1                 types[exp_len] = 'N';
; 0000 00A2                 exp_len ++;
; 0000 00A3                 num = 0;
; 0000 00A4             }
; 0000 00A5 
; 0000 00A6             if (ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7'){
_0x27:
	CPI  R17,49
	BREQ _0x29
	CPI  R17,50
	BREQ _0x29
	CPI  R17,51
	BREQ _0x29
	CPI  R17,52
	BREQ _0x29
	CPI  R17,53
	BREQ _0x29
	CPI  R17,54
	BREQ _0x29
	CPI  R17,55
	BREQ _0x29
	RJMP _0x28
_0x29:
; 0000 00A7                 state =  DEFAULT;
	CALL SUBOPT_0x12
; 0000 00A8                 sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
	CALL SUBOPT_0x1
; 0000 00A9                 if (ch == '1'){
	CPI  R17,49
	BRNE _0x2B
; 0000 00AA                     sprintf(expr, "%s+", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,195
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00AB                     types[exp_len] = '+';
	LDI  R30,LOW(43)
	ST   X,R30
; 0000 00AC                 }
; 0000 00AD                 if (ch == '2') {
_0x2B:
	CPI  R17,50
	BRNE _0x2C
; 0000 00AE                     sprintf(expr, "%s-", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,199
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00AF                     types[exp_len] = '-';
	LDI  R30,LOW(45)
	ST   X,R30
; 0000 00B0                 }
; 0000 00B1                 if (ch == '3') {
_0x2C:
	CPI  R17,51
	BRNE _0x2D
; 0000 00B2                     sprintf(expr, "%s/", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,203
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00B3                     types[exp_len] = '/';
	LDI  R30,LOW(47)
	ST   X,R30
; 0000 00B4                 }
; 0000 00B5                 if (ch == '4') {
_0x2D:
	CPI  R17,52
	BRNE _0x2E
; 0000 00B6                     sprintf(expr, "%s*", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,207
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00B7                     types[exp_len] = '*';
	LDI  R30,LOW(42)
	ST   X,R30
; 0000 00B8                  }
; 0000 00B9                 if (ch == '5') {
_0x2E:
	CPI  R17,53
	BRNE _0x2F
; 0000 00BA                     sprintf(expr, "%s^", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,211
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00BB                     types[exp_len] = '^';
	LDI  R30,LOW(94)
	ST   X,R30
; 0000 00BC                 }
; 0000 00BD                 if (ch == '6') {
_0x2F:
	CPI  R17,54
	BRNE _0x30
; 0000 00BE                     sprintf(expr, "%s(", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,215
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00BF                     types[exp_len] = '(';
	LDI  R30,LOW(40)
	ST   X,R30
; 0000 00C0                  }
; 0000 00C1                 if (ch == '7')  {
_0x30:
	CPI  R17,55
	BRNE _0x31
; 0000 00C2                     sprintf(expr, "%s)", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,219
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00C3                     types[exp_len] = ')';
	LDI  R30,LOW(41)
	ST   X,R30
; 0000 00C4                  }
; 0000 00C5                 strcpy(expression, expr);
_0x31:
	CALL SUBOPT_0x2
	CALL SUBOPT_0x11
; 0000 00C6                 exp_len ++;
	MOVW R30,R8
	ADIW R30,1
	MOVW R8,R30
; 0000 00C7             }
; 0000 00C8             break;
_0x28:
	RJMP _0x10
; 0000 00C9 
; 0000 00CA         case GEO:
_0x26:
	CPI  R30,LOW(0x5)
	LDI  R26,HIGH(0x5)
	CPC  R31,R26
	BREQ PC+2
	RJMP _0x32
; 0000 00CB            if (num_flag == 1){
	CALL SUBOPT_0x8
	BRNE _0x33
; 0000 00CC                 num_flag = 0;
	CLR  R10
	CLR  R11
; 0000 00CD                 val[exp_len] = num;
	CALL SUBOPT_0x9
; 0000 00CE                 types[exp_len] = 'N';
	CALL SUBOPT_0x13
	CALL SUBOPT_0x14
; 0000 00CF                 exp_len ++;
; 0000 00D0                 num = 0;
; 0000 00D1             }
; 0000 00D2             if (ch == '1' || ch == '2' || ch == '3'){
_0x33:
	CPI  R17,49
	BREQ _0x35
	CPI  R17,50
	BREQ _0x35
	CPI  R17,51
	BRNE _0x34
_0x35:
; 0000 00D3                 state =  DEFAULT;
	CALL SUBOPT_0x12
; 0000 00D4                 sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
	CALL SUBOPT_0x1
; 0000 00D5                 if (ch == '1') {
	CPI  R17,49
	BRNE _0x37
; 0000 00D6                     sprintf(expr, "%ssin", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,223
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00D7                     types[exp_len] = 's';
	LDI  R30,LOW(115)
	ST   X,R30
; 0000 00D8                 }
; 0000 00D9                 if (ch == '2') {
_0x37:
	CPI  R17,50
	BRNE _0x38
; 0000 00DA                     sprintf(expr, "%scos", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,229
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00DB                     types[exp_len] = 'c';
	LDI  R30,LOW(99)
	ST   X,R30
; 0000 00DC                 }
; 0000 00DD                 if (ch == '3') {
_0x38:
	CPI  R17,51
	BRNE _0x39
; 0000 00DE                     sprintf(expr, "%stan", expression);
	CALL SUBOPT_0xE
	__POINTW1FN _0x0,235
	CALL SUBOPT_0xF
	CALL SUBOPT_0x15
; 0000 00DF                     types[exp_len] = 't';
	LDI  R30,LOW(116)
	ST   X,R30
; 0000 00E0                 }
; 0000 00E1                 strcpy(expression, expr);
_0x39:
	CALL SUBOPT_0x2
	CALL SUBOPT_0x11
; 0000 00E2                 exp_len ++;
	MOVW R30,R8
	ADIW R30,1
	MOVW R8,R30
; 0000 00E3             }
; 0000 00E4             break;
_0x34:
	RJMP _0x10
; 0000 00E5 
; 0000 00E6         case RESULT:
_0x32:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x10
; 0000 00E7             if (ch == '*'){
	CPI  R17,42
	BRNE _0x3B
; 0000 00E8                 int i = 0;
; 0000 00E9                 state =  DEFAULT;
	SBIW R28,2
	CALL SUBOPT_0x7
;	expr -> Y+17
;	res -> Y+7
;	result -> Y+3
;	i -> Y+0
	CALL SUBOPT_0x12
; 0000 00EA                 sprintf(msg, "0:Num  1:Basic  2:Adv  3.Geo");
	CALL SUBOPT_0x1
; 0000 00EB                 sprintf(expression, "Expr: ");
	CALL SUBOPT_0x2
	CALL SUBOPT_0x3
; 0000 00EC                 exp_len = 0;
	CLR  R8
	CLR  R9
; 0000 00ED                 for (i = 0; i < 32; i ++){
	LDI  R30,LOW(0)
	STD  Y+0,R30
	STD  Y+0+1,R30
_0x3D:
	LD   R26,Y
	LDD  R27,Y+1
	SBIW R26,32
	BRGE _0x3E
; 0000 00EE                     val[i] = 0.0;
	LD   R30,Y
	LDD  R31,Y+1
	CALL SUBOPT_0x16
	CALL SUBOPT_0x5
; 0000 00EF                     types[i] = 'x';
	LD   R30,Y
	LDD  R31,Y+1
	CALL SUBOPT_0x17
; 0000 00F0                 }
	LD   R30,Y
	LDD  R31,Y+1
	ADIW R30,1
	ST   Y,R30
	STD  Y+1,R31
	RJMP _0x3D
_0x3E:
; 0000 00F1             }
	ADIW R28,2
; 0000 00F2             break;
_0x3B:
; 0000 00F3     }
_0x10:
; 0000 00F4 
; 0000 00F5 }
_0x20C000F:
	LDD  R17,Y+0
	ADIW R28,47
	RET
; .FEND
;
;float evaluate(void)
; 0000 00F8 {
_evaluate:
; .FSTART _evaluate
; 0000 00F9     int i, start, end;
; 0000 00FA     int idx1, idx2;
; 0000 00FB     int token_count = exp_len;
; 0000 00FC     int paren_flag = 0;
; 0000 00FD 
; 0000 00FE     while(token_count > 1)
	SBIW R28,8
	CALL SUBOPT_0x7
	CALL __SAVELOCR6
;	i -> R16,R17
;	start -> R18,R19
;	end -> R20,R21
;	idx1 -> Y+12
;	idx2 -> Y+10
;	token_count -> Y+8
;	paren_flag -> Y+6
	__PUTWSR 8,9,8
_0x3F:
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,2
	BRGE PC+2
	RJMP _0x41
; 0000 00FF     {
; 0000 0100        i = 0;
	__GETWRN 16,17,0
; 0000 0101        start = -1;
	__GETWRN 18,19,-1
; 0000 0102        end = exp_len;
	MOVW R20,R8
; 0000 0103        while(i < end)
_0x42:
	__CPWRR 16,17,20,21
	BRGE _0x44
; 0000 0104         {
; 0000 0105             if (types[i] == '('){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x28)
	BRNE _0x45
; 0000 0106                 paren_flag = 1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STD  Y+6,R30
	STD  Y+6+1,R31
; 0000 0107                 lcd_putchar(i + '0');
	MOV  R26,R16
	SUBI R26,-LOW(48)
	CALL _lcd_putchar
; 0000 0108                 start = i;
	MOVW R18,R16
; 0000 0109             }
; 0000 010A             if (types[i] == ')'){
_0x45:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x29)
	BRNE _0x46
; 0000 010B                 paren_flag = 0;
	LDI  R30,LOW(0)
	STD  Y+6,R30
	STD  Y+6+1,R30
; 0000 010C                 lcd_putchar(i + '0');
	MOV  R26,R16
	SUBI R26,-LOW(48)
	CALL _lcd_putchar
; 0000 010D                 end = i;
	MOVW R20,R16
; 0000 010E                 if (start != -1){
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	CP   R30,R18
	CPC  R31,R19
	BREQ _0x47
; 0000 010F                     types[start] = 'x';
	LDI  R26,LOW(_types)
	LDI  R27,HIGH(_types)
	ADD  R26,R18
	ADC  R27,R19
	LDI  R30,LOW(120)
	ST   X,R30
; 0000 0110                     types[i] = 'x';
	CALL SUBOPT_0x6
	LDI  R30,LOW(120)
	CALL SUBOPT_0x18
; 0000 0111                     token_count -= 2;
; 0000 0112                 }
; 0000 0113                 else{
	RJMP _0x48
_0x47:
; 0000 0114                     return ERROR;
	RJMP _0x20C000E
; 0000 0115                 }
_0x48:
; 0000 0116                 break;
	RJMP _0x44
; 0000 0117             }
; 0000 0118             i ++;
_0x46:
	__ADDWRN 16,17,1
; 0000 0119         }
	RJMP _0x42
_0x44:
; 0000 011A 
; 0000 011B         if (paren_flag == 1)
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	SBIW R26,1
	BRNE _0x49
; 0000 011C             return ERROR;
	RJMP _0x20C000E
; 0000 011D 
; 0000 011E         // neg
; 0000 011F         for (i = start + 1; i < end; i ++)
_0x49:
	MOVW R30,R18
	ADIW R30,1
	MOVW R16,R30
_0x4B:
	__CPWRR 16,17,20,21
	BRGE _0x4C
; 0000 0120         {
; 0000 0121             if (types[i] == 'n'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x6E)
	BRNE _0x4D
; 0000 0122                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 0123                 if (idx2 == -1) return ERROR;
	BRNE _0x4E
	RJMP _0x20C000E
; 0000 0124                 val[i] = -val[idx2];
_0x4E:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	CALL __ANEGF1
	MOVW R26,R0
	CALL SUBOPT_0x1C
; 0000 0125                 types[idx2] = 'x';
; 0000 0126                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 0127                 token_count -= 1;
; 0000 0128             }
; 0000 0129         }
_0x4D:
	__ADDWRN 16,17,1
	RJMP _0x4B
_0x4C:
; 0000 012A 
; 0000 012B 
; 0000 012C         // sin, cos, tan
; 0000 012D         for (i = start + 1; i < end; i ++)
	MOVW R30,R18
	ADIW R30,1
	MOVW R16,R30
_0x50:
	__CPWRR 16,17,20,21
	BRLT PC+2
	RJMP _0x51
; 0000 012E         {
; 0000 012F             if (types[i] == 's'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x73)
	BRNE _0x52
; 0000 0130                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 0131                 if (idx2 == -1) return ERROR;
	BRNE _0x53
	RJMP _0x20C000E
; 0000 0132                 val[i] = sin(val[idx2]);
_0x53:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	MOVW R26,R30
	MOVW R24,R22
	CALL _sin
	POP  R26
	POP  R27
	CALL SUBOPT_0x1C
; 0000 0133                 types[idx2] = 'x';
; 0000 0134                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 0135                 token_count -= 1;
; 0000 0136             }
; 0000 0137 
; 0000 0138             if (types[i] == 'c'){
_0x52:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x63)
	BRNE _0x54
; 0000 0139                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 013A                 if (idx2 == -1) return ERROR;
	BRNE _0x55
	RJMP _0x20C000E
; 0000 013B                 val[i] = cos(val[idx2]);
_0x55:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	MOVW R26,R30
	MOVW R24,R22
	CALL _cos
	POP  R26
	POP  R27
	CALL SUBOPT_0x1C
; 0000 013C                 types[idx2] = 'x';
; 0000 013D                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 013E                 token_count -= 1;
; 0000 013F             }
; 0000 0140 
; 0000 0141             if (types[i] == 't'){
_0x54:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x74)
	BRNE _0x56
; 0000 0142                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 0143                 if (idx2 == -1) return ERROR;
	BRNE _0x57
	RJMP _0x20C000E
; 0000 0144                 val[i] = tan(val[idx2]);
_0x57:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	MOVW R26,R30
	MOVW R24,R22
	CALL _tan
	POP  R26
	POP  R27
	CALL SUBOPT_0x1C
; 0000 0145                 types[idx2] = 'x';
; 0000 0146                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 0147                 token_count -= 1;
; 0000 0148             }
; 0000 0149         }
_0x56:
	__ADDWRN 16,17,1
	RJMP _0x50
_0x51:
; 0000 014A 
; 0000 014B         // exp, log
; 0000 014C         for (i = start + 1; i < end; i ++)
	MOVW R30,R18
	ADIW R30,1
	MOVW R16,R30
_0x59:
	__CPWRR 16,17,20,21
	BRLT PC+2
	RJMP _0x5A
; 0000 014D         {
; 0000 014E             if (types[i] == 'e'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x65)
	BRNE _0x5B
; 0000 014F                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 0150                 if (idx2 == -1) return ERROR;
	BRNE _0x5C
	RJMP _0x20C000E
; 0000 0151                 val[i] = exp(val[idx2]);
_0x5C:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	CALL SUBOPT_0x1E
	POP  R26
	POP  R27
	CALL SUBOPT_0x1C
; 0000 0152                 types[idx2] = 'x';
; 0000 0153                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 0154                 token_count -= 1;
; 0000 0155             }
; 0000 0156 
; 0000 0157             if (types[i] == 'l'){
_0x5B:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x6C)
	BRNE _0x5D
; 0000 0158                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 0159                 if (idx2 == -1) return ERROR;
	BRNE _0x5E
	RJMP _0x20C000E
; 0000 015A                 val[i] = log(val[idx2]);
_0x5E:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	MOVW R26,R30
	MOVW R24,R22
	CALL _log
	POP  R26
	POP  R27
	CALL SUBOPT_0x1C
; 0000 015B                 types[idx2] = 'x';
; 0000 015C                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 015D                 token_count -= 1;
; 0000 015E             }
; 0000 015F         }
_0x5D:
	__ADDWRN 16,17,1
	RJMP _0x59
_0x5A:
; 0000 0160 
; 0000 0161         // pow and sqrt
; 0000 0162         for (i = start + 1; i < end; i ++)
	MOVW R30,R18
	ADIW R30,1
	MOVW R16,R30
_0x60:
	__CPWRR 16,17,20,21
	BRLT PC+2
	RJMP _0x61
; 0000 0163         {
; 0000 0164             if (types[i] == '^'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x5E)
	BRNE _0x62
; 0000 0165                 idx1 = get_last_num(i);
	CALL SUBOPT_0x1F
; 0000 0166                 idx2 = get_next_num(i, end);
; 0000 0167                 if (idx1 == -1 || idx2 == -1) return ERROR;
	BREQ _0x64
	CALL SUBOPT_0x20
	BRNE _0x63
_0x64:
	RJMP _0x20C000E
; 0000 0168                 val[i] = pow(val[idx1], val[idx2]);
_0x63:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x21
	CALL SUBOPT_0x1B
	CALL __PUTPARD1
	LDD  R30,Y+14
	LDD  R31,Y+14+1
	CALL SUBOPT_0x16
	CALL SUBOPT_0x1B
	MOVW R26,R30
	MOVW R24,R22
	CALL _pow
	POP  R26
	POP  R27
	CALL SUBOPT_0x22
; 0000 0169                 types[idx1] = 'x';
; 0000 016A                 types[idx2] = 'x';
	CALL SUBOPT_0x23
; 0000 016B                 types[i] = 'N';
	CALL SUBOPT_0x6
	LDI  R30,LOW(78)
	CALL SUBOPT_0x18
; 0000 016C                 token_count -= 2;
; 0000 016D             }
; 0000 016E 
; 0000 016F             if (types[i] == 'S'){
_0x62:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x53)
	BRNE _0x66
; 0000 0170                 idx2 = get_next_num(i, end);
	CALL SUBOPT_0x19
; 0000 0171                 if (idx2 == -1) return ERROR;
	BRNE _0x67
	RJMP _0x20C000E
; 0000 0172                 val[i] = sqrt(val[idx2]);
_0x67:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	MOVW R26,R30
	MOVW R24,R22
	CALL _sqrt
	POP  R26
	POP  R27
	CALL SUBOPT_0x1C
; 0000 0173                 types[idx2] = 'x';
; 0000 0174                 types[i] = 'N';
	CALL SUBOPT_0x6
	CALL SUBOPT_0x1D
; 0000 0175                 token_count -= 1;
; 0000 0176             }
; 0000 0177         }
_0x66:
	__ADDWRN 16,17,1
	RJMP _0x60
_0x61:
; 0000 0178 
; 0000 0179 
; 0000 017A         // * and /
; 0000 017B         for (i = start + 1; i < end; i ++)
	MOVW R30,R18
	ADIW R30,1
	MOVW R16,R30
_0x69:
	__CPWRR 16,17,20,21
	BRLT PC+2
	RJMP _0x6A
; 0000 017C         {
; 0000 017D             if (types[i] == '*'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x2A)
	BRNE _0x6B
; 0000 017E                 idx1 = get_last_num(i);
	CALL SUBOPT_0x1F
; 0000 017F                 idx2 = get_next_num(i, end);
; 0000 0180                 if (idx1 == -1 || idx2 == -1) return ERROR;
	BREQ _0x6D
	CALL SUBOPT_0x20
	BRNE _0x6C
_0x6D:
	RJMP _0x20C000E
; 0000 0181                 val[i] = val[idx1] * val[idx2];
_0x6C:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x21
	CALL SUBOPT_0x1B
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __MULF12
	POP  R26
	POP  R27
	CALL SUBOPT_0x22
; 0000 0182                 types[idx1] = 'x';
; 0000 0183                 types[idx2] = 'x';
	CALL SUBOPT_0x23
; 0000 0184                 types[i] = 'N';
	CALL SUBOPT_0x6
	LDI  R30,LOW(78)
	CALL SUBOPT_0x18
; 0000 0185                 token_count -= 2;
; 0000 0186             }
; 0000 0187 
; 0000 0188             if (types[i] == '/'){
_0x6B:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x2F)
	BRNE _0x6F
; 0000 0189                 idx1 = get_last_num(i);
	CALL SUBOPT_0x1F
; 0000 018A                 idx2 = get_next_num(i, end);
; 0000 018B                 if (idx1 == -1 || idx2 == -1) return ERROR;
	BREQ _0x71
	CALL SUBOPT_0x20
	BRNE _0x70
_0x71:
_0x20C000E:
	__GETD1N 0x4CEB79A3
	CALL __LOADLOCR6
	ADIW R28,14
	RET
; 0000 018C                 val[i] = val[idx1] / val[idx2];
_0x70:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x21
	CALL SUBOPT_0x1B
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __DIVF21
	POP  R26
	POP  R27
	CALL SUBOPT_0x22
; 0000 018D                 types[idx1] = 'x';
; 0000 018E                 types[idx2] = 'x';
	CALL SUBOPT_0x23
; 0000 018F                 types[i] = 'N';
	CALL SUBOPT_0x6
	LDI  R30,LOW(78)
	CALL SUBOPT_0x18
; 0000 0190                 token_count -= 2;
; 0000 0191             }
; 0000 0192         }
_0x6F:
	__ADDWRN 16,17,1
	RJMP _0x69
_0x6A:
; 0000 0193 
; 0000 0194         // + and -
; 0000 0195         for (i = start + 1; i < end; i ++)
	MOVW R30,R18
	ADIW R30,1
	MOVW R16,R30
_0x74:
	__CPWRR 16,17,20,21
	BRLT PC+2
	RJMP _0x75
; 0000 0196         {
; 0000 0197             if (types[i] == '+'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x2B)
	BRNE _0x76
; 0000 0198                 idx1 = get_last_num(i);
	CALL SUBOPT_0x1F
; 0000 0199                 idx2 = get_next_num(i, end);
; 0000 019A                 if (idx1 == -1 || idx2 == -1) return ERROR;
	BREQ _0x78
	CALL SUBOPT_0x20
	BRNE _0x77
_0x78:
	CALL SUBOPT_0x24
	JMP  _0x20C0005
; 0000 019B                 val[i] = val[idx1] + val[idx2];
_0x77:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x21
	CALL SUBOPT_0x1B
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __ADDF12
	POP  R26
	POP  R27
	CALL SUBOPT_0x22
; 0000 019C                 types[idx1] = 'x';
; 0000 019D                 types[idx2] = 'x';
	CALL SUBOPT_0x23
; 0000 019E                 types[i] = 'N';
	CALL SUBOPT_0x6
	LDI  R30,LOW(78)
	CALL SUBOPT_0x18
; 0000 019F                 token_count -= 2;
; 0000 01A0             }
; 0000 01A1 
; 0000 01A2             if (types[i] == '-'){
_0x76:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x2D)
	BRNE _0x7A
; 0000 01A3                 idx1 = get_last_num(i);
	CALL SUBOPT_0x1F
; 0000 01A4                 idx2 = get_next_num(i, end);
; 0000 01A5                 if (idx1 == -1 || idx2 == -1) return ERROR;
	BREQ _0x7C
	CALL SUBOPT_0x20
	BRNE _0x7B
_0x7C:
	CALL SUBOPT_0x24
	JMP  _0x20C0005
; 0000 01A6                 val[i] = val[idx1] - val[idx2];
_0x7B:
	CALL SUBOPT_0x4
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x21
	CALL SUBOPT_0x1B
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x1A
	CALL SUBOPT_0x1B
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL SUBOPT_0x25
	POP  R26
	POP  R27
	CALL SUBOPT_0x22
; 0000 01A7                 types[idx1] = 'x';
; 0000 01A8                 types[idx2] = 'x';
	CALL SUBOPT_0x23
; 0000 01A9                 types[i] = 'N';
	CALL SUBOPT_0x6
	LDI  R30,LOW(78)
	CALL SUBOPT_0x18
; 0000 01AA                 token_count -= 2;
; 0000 01AB             }
; 0000 01AC         }
_0x7A:
	__ADDWRN 16,17,1
	RJMP _0x74
_0x75:
; 0000 01AD     }
	RJMP _0x3F
_0x41:
; 0000 01AE     for (i = 0; i < exp_len; i ++)
	__GETWRN 16,17,0
_0x7F:
	__CPWRR 16,17,8,9
	BRGE _0x80
; 0000 01AF     {
; 0000 01B0          if (types[i] == 'N'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x4E)
	BRNE _0x81
; 0000 01B1             return val[i];
	CALL SUBOPT_0x4
	CALL SUBOPT_0x1B
	CALL __LOADLOCR6
	JMP  _0x20C0005
; 0000 01B2          }
; 0000 01B3     }
_0x81:
	__ADDWRN 16,17,1
	RJMP _0x7F
_0x80:
; 0000 01B4 
; 0000 01B5     return val[0];
	LDS  R30,_val
	LDS  R31,_val+1
	LDS  R22,_val+2
	LDS  R23,_val+3
	CALL __LOADLOCR6
	JMP  _0x20C0005
; 0000 01B6 
; 0000 01B7 }
; .FEND
;
;int get_last_num(int index)
; 0000 01BA {
_get_last_num:
; .FSTART _get_last_num
; 0000 01BB     int i = 0;
; 0000 01BC     for (i = index - 1 ; i >= 0; i --)
	CALL SUBOPT_0x26
;	index -> Y+2
;	i -> R16,R17
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	SBIW R30,1
	MOVW R16,R30
_0x83:
	TST  R17
	BRMI _0x84
; 0000 01BD     {
; 0000 01BE         if (types[i] == 'N'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x4E)
	BRNE _0x85
; 0000 01BF             return  i;
	MOVW R30,R16
	RJMP _0x20C000D
; 0000 01C0         } else if (types[i] != 'x'){
_0x85:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x78)
	BREQ _0x87
; 0000 01C1             return -1;
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
_0x20C000D:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,4
	RET
; 0000 01C2         }
; 0000 01C3     }
_0x87:
	__SUBWRN 16,17,1
	RJMP _0x83
_0x84:
; 0000 01C4     return -1;
	CALL SUBOPT_0x27
	JMP  _0x20C0002
; 0000 01C5 }
; .FEND
;
;
;int get_next_num(int index, int end)
; 0000 01C9 {
_get_next_num:
; .FSTART _get_next_num
; 0000 01CA     int i = 0;
; 0000 01CB     for (i = index + 1; i < end; i ++)
	CALL SUBOPT_0x26
;	index -> Y+4
;	end -> Y+2
;	i -> R16,R17
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	ADIW R30,1
	MOVW R16,R30
_0x89:
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	CP   R16,R30
	CPC  R17,R31
	BRGE _0x8A
; 0000 01CC     {
; 0000 01CD         if (types[i] == 'N'){
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x4E)
	BRNE _0x8B
; 0000 01CE             return  i;
	MOVW R30,R16
	LDD  R17,Y+1
	LDD  R16,Y+0
	RJMP _0x20C000C
; 0000 01CF         }  else if (types[i] != 'x'){
_0x8B:
	CALL SUBOPT_0x6
	LD   R26,X
	CPI  R26,LOW(0x78)
	BREQ _0x8D
; 0000 01D0             return -1;
	CALL SUBOPT_0x27
	RJMP _0x20C000C
; 0000 01D1         }
; 0000 01D2     }
_0x8D:
	__ADDWRN 16,17,1
	RJMP _0x89
_0x8A:
; 0000 01D3     return -1;
	CALL SUBOPT_0x27
	RJMP _0x20C000C
; 0000 01D4 }
; .FEND
;
;char keypad(void)
; 0000 01D7 {
_keypad:
; .FSTART _keypad
; 0000 01D8     int row = 0, column = -1, position = 0;
; 0000 01D9     while(1){
	CALL __SAVELOCR6
;	row -> R16,R17
;	column -> R18,R19
;	position -> R20,R21
	__GETWRN 16,17,0
	__GETWRN 18,19,-1
	__GETWRN 20,21,0
_0x8E:
; 0000 01DA         for (row = 0; row < 4; row ++){
	__GETWRN 16,17,0
_0x92:
	__CPWRN 16,17,4
	BRGE _0x93
; 0000 01DB             PORTD = shift[row];
	MOVW R30,R16
	SUBI R30,LOW(-_shift*2)
	SBCI R31,HIGH(-_shift*2)
	LPM  R0,Z
	OUT  0x12,R0
; 0000 01DC             if (PIND.2 == 0) {
	SBIC 0x10,2
	RJMP _0x94
; 0000 01DD                 column = 0;
	__GETWRN 18,19,0
; 0000 01DE             }
; 0000 01DF             if (PIND.1 == 0) {
_0x94:
	SBIC 0x10,1
	RJMP _0x95
; 0000 01E0                 column = 1;
	__GETWRN 18,19,1
; 0000 01E1             }
; 0000 01E2             if (PIND.0 == 0) {
_0x95:
	SBIC 0x10,0
	RJMP _0x96
; 0000 01E3                 column = 2;
	__GETWRN 18,19,2
; 0000 01E4             }
; 0000 01E5             if (column != -1) {
_0x96:
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	CP   R30,R18
	CPC  R31,R19
	BREQ _0x97
; 0000 01E6                 position = row * 3  + column;
	MOVW R30,R16
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12
	ADD  R30,R18
	ADC  R31,R19
	MOVW R20,R30
; 0000 01E7                 column = -1;
	__GETWRN 18,19,-1
; 0000 01E8                 while(PIND.2 == 0);
_0x98:
	SBIS 0x10,2
	RJMP _0x98
; 0000 01E9                 while(PIND.1 == 0);
_0x9B:
	SBIS 0x10,1
	RJMP _0x9B
; 0000 01EA                 while(PIND.0 == 0);
_0x9E:
	SBIS 0x10,0
	RJMP _0x9E
; 0000 01EB                 return layout[position];
	MOVW R30,R20
	SUBI R30,LOW(-_layout*2)
	SBCI R31,HIGH(-_layout*2)
	LPM  R30,Z
	RJMP _0x20C000B
; 0000 01EC             }
; 0000 01ED             delay_ms(5);
_0x97:
	LDI  R26,LOW(5)
	LDI  R27,0
	CALL _delay_ms
; 0000 01EE         }
	__ADDWRN 16,17,1
	RJMP _0x92
_0x93:
; 0000 01EF     }
	RJMP _0x8E
; 0000 01F0 }
_0x20C000B:
	CALL __LOADLOCR6
_0x20C000C:
	ADIW R28,6
	RET
; .FEND
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x40
	.EQU __sm_mask=0xB0
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0xA0
	.EQU __sm_ext_standby=0xB0
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.DSEG

	.CSEG
__lcd_write_nibble_G100:
; .FSTART __lcd_write_nibble_G100
	ST   -Y,R26
	IN   R30,0x1B
	ANDI R30,LOW(0xF)
	MOV  R26,R30
	LD   R30,Y
	ANDI R30,LOW(0xF0)
	OR   R30,R26
	OUT  0x1B,R30
	__DELAY_USB 7
	SBI  0x1B,2
	__DELAY_USB 7
	CBI  0x1B,2
	__DELAY_USB 7
	RJMP _0x20C000A
; .FEND
__lcd_write_data:
; .FSTART __lcd_write_data
	ST   -Y,R26
	LD   R26,Y
	RCALL __lcd_write_nibble_G100
    ld    r30,y
    swap  r30
    st    y,r30
	LD   R26,Y
	RCALL __lcd_write_nibble_G100
	__DELAY_USB 67
	RJMP _0x20C000A
; .FEND
_lcd_gotoxy:
; .FSTART _lcd_gotoxy
	ST   -Y,R26
	LD   R30,Y
	LDI  R31,0
	SUBI R30,LOW(-__base_y_G100)
	SBCI R31,HIGH(-__base_y_G100)
	LD   R30,Z
	LDD  R26,Y+1
	ADD  R26,R30
	RCALL __lcd_write_data
	LDD  R13,Y+1
	LDD  R12,Y+0
	ADIW R28,2
	RET
; .FEND
_lcd_clear:
; .FSTART _lcd_clear
	LDI  R26,LOW(2)
	CALL SUBOPT_0x28
	LDI  R26,LOW(12)
	RCALL __lcd_write_data
	LDI  R26,LOW(1)
	CALL SUBOPT_0x28
	LDI  R30,LOW(0)
	MOV  R12,R30
	MOV  R13,R30
	RET
; .FEND
_lcd_putchar:
; .FSTART _lcd_putchar
	ST   -Y,R26
	LD   R26,Y
	CPI  R26,LOW(0xA)
	BREQ _0x2000005
	LDS  R30,__lcd_maxx
	CP   R13,R30
	BRLO _0x2000004
_0x2000005:
	LDI  R30,LOW(0)
	ST   -Y,R30
	INC  R12
	MOV  R26,R12
	RCALL _lcd_gotoxy
	LD   R26,Y
	CPI  R26,LOW(0xA)
	BRNE _0x2000007
	RJMP _0x20C000A
_0x2000007:
_0x2000004:
	INC  R13
	SBI  0x1B,0
	LD   R26,Y
	RCALL __lcd_write_data
	CBI  0x1B,0
	RJMP _0x20C000A
; .FEND
_lcd_puts:
; .FSTART _lcd_puts
	ST   -Y,R27
	ST   -Y,R26
	ST   -Y,R17
_0x2000008:
	LDD  R26,Y+1
	LDD  R27,Y+1+1
	LD   R30,X+
	STD  Y+1,R26
	STD  Y+1+1,R27
	MOV  R17,R30
	CPI  R30,0
	BREQ _0x200000A
	MOV  R26,R17
	RCALL _lcd_putchar
	RJMP _0x2000008
_0x200000A:
	LDD  R17,Y+0
	ADIW R28,3
	RET
; .FEND
_lcd_init:
; .FSTART _lcd_init
	ST   -Y,R26
	IN   R30,0x1A
	ORI  R30,LOW(0xF0)
	OUT  0x1A,R30
	SBI  0x1A,2
	SBI  0x1A,0
	SBI  0x1A,1
	CBI  0x1B,2
	CBI  0x1B,0
	CBI  0x1B,1
	LD   R30,Y
	STS  __lcd_maxx,R30
	SUBI R30,-LOW(128)
	__PUTB1MN __base_y_G100,2
	LD   R30,Y
	SUBI R30,-LOW(192)
	__PUTB1MN __base_y_G100,3
	LDI  R26,LOW(20)
	LDI  R27,0
	CALL _delay_ms
	CALL SUBOPT_0x29
	CALL SUBOPT_0x29
	CALL SUBOPT_0x29
	LDI  R26,LOW(32)
	RCALL __lcd_write_nibble_G100
	__DELAY_USB 133
	LDI  R26,LOW(40)
	RCALL __lcd_write_data
	LDI  R26,LOW(4)
	RCALL __lcd_write_data
	LDI  R26,LOW(133)
	RCALL __lcd_write_data
	LDI  R26,LOW(6)
	RCALL __lcd_write_data
	RCALL _lcd_clear
_0x20C000A:
	ADIW R28,1
	RET
; .FEND
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x40
	.EQU __sm_mask=0xB0
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0xA0
	.EQU __sm_ext_standby=0xB0
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG
_put_buff_G101:
; .FSTART _put_buff_G101
	ST   -Y,R27
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,2
	CALL __GETW1P
	SBIW R30,0
	BREQ _0x2020010
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,4
	CALL __GETW1P
	MOVW R16,R30
	SBIW R30,0
	BREQ _0x2020012
	__CPWRN 16,17,2
	BRLO _0x2020013
	MOVW R30,R16
	SBIW R30,1
	MOVW R16,R30
	__PUTW1SNS 2,4
_0x2020012:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,2
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	SBIW R30,1
	LDD  R26,Y+4
	STD  Z+0,R26
_0x2020013:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	CALL __GETW1P
	TST  R31
	BRMI _0x2020014
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
_0x2020014:
	RJMP _0x2020015
_0x2020010:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	ST   X+,R30
	ST   X,R31
_0x2020015:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,5
	RET
; .FEND
__print_G101:
; .FSTART __print_G101
	ST   -Y,R27
	ST   -Y,R26
	SBIW R28,6
	CALL __SAVELOCR6
	LDI  R17,0
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	ST   X+,R30
	ST   X,R31
_0x2020016:
	LDD  R30,Y+18
	LDD  R31,Y+18+1
	ADIW R30,1
	STD  Y+18,R30
	STD  Y+18+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R18,R30
	CPI  R30,0
	BRNE PC+2
	RJMP _0x2020018
	MOV  R30,R17
	CPI  R30,0
	BRNE _0x202001C
	CPI  R18,37
	BRNE _0x202001D
	LDI  R17,LOW(1)
	RJMP _0x202001E
_0x202001D:
	CALL SUBOPT_0x2A
_0x202001E:
	RJMP _0x202001B
_0x202001C:
	CPI  R30,LOW(0x1)
	BRNE _0x202001F
	CPI  R18,37
	BRNE _0x2020020
	CALL SUBOPT_0x2A
	RJMP _0x20200CC
_0x2020020:
	LDI  R17,LOW(2)
	LDI  R20,LOW(0)
	LDI  R16,LOW(0)
	CPI  R18,45
	BRNE _0x2020021
	LDI  R16,LOW(1)
	RJMP _0x202001B
_0x2020021:
	CPI  R18,43
	BRNE _0x2020022
	LDI  R20,LOW(43)
	RJMP _0x202001B
_0x2020022:
	CPI  R18,32
	BRNE _0x2020023
	LDI  R20,LOW(32)
	RJMP _0x202001B
_0x2020023:
	RJMP _0x2020024
_0x202001F:
	CPI  R30,LOW(0x2)
	BRNE _0x2020025
_0x2020024:
	LDI  R21,LOW(0)
	LDI  R17,LOW(3)
	CPI  R18,48
	BRNE _0x2020026
	ORI  R16,LOW(128)
	RJMP _0x202001B
_0x2020026:
	RJMP _0x2020027
_0x2020025:
	CPI  R30,LOW(0x3)
	BREQ PC+2
	RJMP _0x202001B
_0x2020027:
	CPI  R18,48
	BRLO _0x202002A
	CPI  R18,58
	BRLO _0x202002B
_0x202002A:
	RJMP _0x2020029
_0x202002B:
	LDI  R26,LOW(10)
	MUL  R21,R26
	MOV  R21,R0
	MOV  R30,R18
	SUBI R30,LOW(48)
	ADD  R21,R30
	RJMP _0x202001B
_0x2020029:
	MOV  R30,R18
	CPI  R30,LOW(0x63)
	BRNE _0x202002F
	CALL SUBOPT_0x2B
	LDD  R30,Y+16
	LDD  R31,Y+16+1
	LDD  R26,Z+4
	ST   -Y,R26
	CALL SUBOPT_0x2C
	RJMP _0x2020030
_0x202002F:
	CPI  R30,LOW(0x73)
	BRNE _0x2020032
	CALL SUBOPT_0x2B
	CALL SUBOPT_0x2D
	CALL _strlen
	MOV  R17,R30
	RJMP _0x2020033
_0x2020032:
	CPI  R30,LOW(0x70)
	BRNE _0x2020035
	CALL SUBOPT_0x2B
	CALL SUBOPT_0x2D
	CALL _strlenf
	MOV  R17,R30
	ORI  R16,LOW(8)
_0x2020033:
	ORI  R16,LOW(2)
	ANDI R16,LOW(127)
	LDI  R19,LOW(0)
	RJMP _0x2020036
_0x2020035:
	CPI  R30,LOW(0x64)
	BREQ _0x2020039
	CPI  R30,LOW(0x69)
	BRNE _0x202003A
_0x2020039:
	ORI  R16,LOW(4)
	RJMP _0x202003B
_0x202003A:
	CPI  R30,LOW(0x75)
	BRNE _0x202003C
_0x202003B:
	LDI  R30,LOW(_tbl10_G101*2)
	LDI  R31,HIGH(_tbl10_G101*2)
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDI  R17,LOW(5)
	RJMP _0x202003D
_0x202003C:
	CPI  R30,LOW(0x58)
	BRNE _0x202003F
	ORI  R16,LOW(8)
	RJMP _0x2020040
_0x202003F:
	CPI  R30,LOW(0x78)
	BREQ PC+2
	RJMP _0x2020071
_0x2020040:
	LDI  R30,LOW(_tbl16_G101*2)
	LDI  R31,HIGH(_tbl16_G101*2)
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDI  R17,LOW(4)
_0x202003D:
	SBRS R16,2
	RJMP _0x2020042
	CALL SUBOPT_0x2B
	CALL SUBOPT_0x2E
	LDD  R26,Y+11
	TST  R26
	BRPL _0x2020043
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	CALL __ANEGW1
	STD  Y+10,R30
	STD  Y+10+1,R31
	LDI  R20,LOW(45)
_0x2020043:
	CPI  R20,0
	BREQ _0x2020044
	SUBI R17,-LOW(1)
	RJMP _0x2020045
_0x2020044:
	ANDI R16,LOW(251)
_0x2020045:
	RJMP _0x2020046
_0x2020042:
	CALL SUBOPT_0x2B
	CALL SUBOPT_0x2E
_0x2020046:
_0x2020036:
	SBRC R16,0
	RJMP _0x2020047
_0x2020048:
	CP   R17,R21
	BRSH _0x202004A
	SBRS R16,7
	RJMP _0x202004B
	SBRS R16,2
	RJMP _0x202004C
	ANDI R16,LOW(251)
	MOV  R18,R20
	SUBI R17,LOW(1)
	RJMP _0x202004D
_0x202004C:
	LDI  R18,LOW(48)
_0x202004D:
	RJMP _0x202004E
_0x202004B:
	LDI  R18,LOW(32)
_0x202004E:
	CALL SUBOPT_0x2A
	SUBI R21,LOW(1)
	RJMP _0x2020048
_0x202004A:
_0x2020047:
	MOV  R19,R17
	SBRS R16,1
	RJMP _0x202004F
_0x2020050:
	CPI  R19,0
	BREQ _0x2020052
	SBRS R16,3
	RJMP _0x2020053
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	LPM  R18,Z+
	STD  Y+6,R30
	STD  Y+6+1,R31
	RJMP _0x2020054
_0x2020053:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LD   R18,X+
	STD  Y+6,R26
	STD  Y+6+1,R27
_0x2020054:
	CALL SUBOPT_0x2A
	CPI  R21,0
	BREQ _0x2020055
	SUBI R21,LOW(1)
_0x2020055:
	SUBI R19,LOW(1)
	RJMP _0x2020050
_0x2020052:
	RJMP _0x2020056
_0x202004F:
_0x2020058:
	LDI  R18,LOW(48)
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	CALL __GETW1PF
	STD  Y+8,R30
	STD  Y+8+1,R31
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,2
	STD  Y+6,R30
	STD  Y+6+1,R31
_0x202005A:
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CP   R26,R30
	CPC  R27,R31
	BRLO _0x202005C
	SUBI R18,-LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	SUB  R30,R26
	SBC  R31,R27
	STD  Y+10,R30
	STD  Y+10+1,R31
	RJMP _0x202005A
_0x202005C:
	CPI  R18,58
	BRLO _0x202005D
	SBRS R16,3
	RJMP _0x202005E
	SUBI R18,-LOW(7)
	RJMP _0x202005F
_0x202005E:
	SUBI R18,-LOW(39)
_0x202005F:
_0x202005D:
	SBRC R16,4
	RJMP _0x2020061
	CPI  R18,49
	BRSH _0x2020063
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,1
	BRNE _0x2020062
_0x2020063:
	RJMP _0x20200CD
_0x2020062:
	CP   R21,R19
	BRLO _0x2020067
	SBRS R16,0
	RJMP _0x2020068
_0x2020067:
	RJMP _0x2020066
_0x2020068:
	LDI  R18,LOW(32)
	SBRS R16,7
	RJMP _0x2020069
	LDI  R18,LOW(48)
_0x20200CD:
	ORI  R16,LOW(16)
	SBRS R16,2
	RJMP _0x202006A
	ANDI R16,LOW(251)
	ST   -Y,R20
	CALL SUBOPT_0x2C
	CPI  R21,0
	BREQ _0x202006B
	SUBI R21,LOW(1)
_0x202006B:
_0x202006A:
_0x2020069:
_0x2020061:
	CALL SUBOPT_0x2A
	CPI  R21,0
	BREQ _0x202006C
	SUBI R21,LOW(1)
_0x202006C:
_0x2020066:
	SUBI R19,LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,2
	BRLO _0x2020059
	RJMP _0x2020058
_0x2020059:
_0x2020056:
	SBRS R16,0
	RJMP _0x202006D
_0x202006E:
	CPI  R21,0
	BREQ _0x2020070
	SUBI R21,LOW(1)
	LDI  R30,LOW(32)
	ST   -Y,R30
	CALL SUBOPT_0x2C
	RJMP _0x202006E
_0x2020070:
_0x202006D:
_0x2020071:
_0x2020030:
_0x20200CC:
	LDI  R17,LOW(0)
_0x202001B:
	RJMP _0x2020016
_0x2020018:
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	CALL __GETW1P
	CALL __LOADLOCR6
	ADIW R28,20
	RET
; .FEND
_sprintf:
; .FSTART _sprintf
	PUSH R15
	MOV  R15,R24
	SBIW R28,6
	CALL __SAVELOCR4
	CALL SUBOPT_0x2F
	SBIW R30,0
	BRNE _0x2020072
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	RJMP _0x20C0009
_0x2020072:
	MOVW R26,R28
	ADIW R26,6
	CALL __ADDW2R15
	MOVW R16,R26
	CALL SUBOPT_0x2F
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDI  R30,LOW(0)
	STD  Y+8,R30
	STD  Y+8+1,R30
	MOVW R26,R28
	ADIW R26,10
	CALL __ADDW2R15
	CALL __GETW1P
	ST   -Y,R31
	ST   -Y,R30
	ST   -Y,R17
	ST   -Y,R16
	LDI  R30,LOW(_put_buff_G101)
	LDI  R31,HIGH(_put_buff_G101)
	ST   -Y,R31
	ST   -Y,R30
	MOVW R26,R28
	ADIW R26,10
	RCALL __print_G101
	MOVW R18,R30
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LDI  R30,LOW(0)
	ST   X,R30
	MOVW R30,R18
_0x20C0009:
	CALL __LOADLOCR4
	ADIW R28,10
	POP  R15
	RET
; .FEND

	.CSEG
_strcpy:
; .FSTART _strcpy
	ST   -Y,R27
	ST   -Y,R26
    ld   r30,y+
    ld   r31,y+
    ld   r26,y+
    ld   r27,y+
    movw r24,r26
strcpy0:
    ld   r22,z+
    st   x+,r22
    tst  r22
    brne strcpy0
    movw r30,r24
    ret
; .FEND
_strcpyf:
; .FSTART _strcpyf
	ST   -Y,R27
	ST   -Y,R26
    ld   r30,y+
    ld   r31,y+
    ld   r26,y+
    ld   r27,y+
    movw r24,r26
strcpyf0:
	lpm  r0,z+
    st   x+,r0
    tst  r0
    brne strcpyf0
    movw r30,r24
    ret
; .FEND
_strlen:
; .FSTART _strlen
	ST   -Y,R27
	ST   -Y,R26
    ld   r26,y+
    ld   r27,y+
    clr  r30
    clr  r31
strlen0:
    ld   r22,x+
    tst  r22
    breq strlen1
    adiw r30,1
    rjmp strlen0
strlen1:
    ret
; .FEND
_strlenf:
; .FSTART _strlenf
	ST   -Y,R27
	ST   -Y,R26
    clr  r26
    clr  r27
    ld   r30,y+
    ld   r31,y+
strlenf0:
	lpm  r0,z+
    tst  r0
    breq strlenf1
    adiw r26,1
    rjmp strlenf0
strlenf1:
    movw r30,r26
    ret
; .FEND

	.CSEG
_ftoa:
; .FSTART _ftoa
	ST   -Y,R27
	ST   -Y,R26
	SBIW R28,4
	CALL SUBOPT_0x7
	LDI  R30,LOW(0)
	STD  Y+2,R30
	LDI  R30,LOW(63)
	STD  Y+3,R30
	ST   -Y,R17
	ST   -Y,R16
	LDD  R30,Y+11
	LDD  R31,Y+11+1
	CPI  R30,LOW(0xFFFF)
	LDI  R26,HIGH(0xFFFF)
	CPC  R31,R26
	BRNE _0x206000D
	CALL SUBOPT_0x30
	__POINTW2FN _0x2060000,0
	CALL _strcpyf
	RJMP _0x20C0008
_0x206000D:
	CPI  R30,LOW(0x7FFF)
	LDI  R26,HIGH(0x7FFF)
	CPC  R31,R26
	BRNE _0x206000C
	CALL SUBOPT_0x30
	__POINTW2FN _0x2060000,1
	CALL _strcpyf
	RJMP _0x20C0008
_0x206000C:
	LDD  R26,Y+12
	TST  R26
	BRPL _0x206000F
	__GETD1S 9
	CALL __ANEGF1
	CALL SUBOPT_0x31
	CALL SUBOPT_0x32
	LDI  R30,LOW(45)
	ST   X,R30
_0x206000F:
	LDD  R26,Y+8
	CPI  R26,LOW(0x7)
	BRLO _0x2060010
	LDI  R30,LOW(6)
	STD  Y+8,R30
_0x2060010:
	LDD  R17,Y+8
_0x2060011:
	MOV  R30,R17
	SUBI R17,1
	CPI  R30,0
	BREQ _0x2060013
	CALL SUBOPT_0x33
	CALL SUBOPT_0x34
	CALL SUBOPT_0x35
	RJMP _0x2060011
_0x2060013:
	CALL SUBOPT_0x36
	CALL __ADDF12
	CALL SUBOPT_0x31
	LDI  R17,LOW(0)
	CALL SUBOPT_0x37
	CALL SUBOPT_0x35
_0x2060014:
	CALL SUBOPT_0x36
	CALL __CMPF12
	BRLO _0x2060016
	CALL SUBOPT_0x33
	CALL SUBOPT_0x38
	CALL SUBOPT_0x35
	SUBI R17,-LOW(1)
	CPI  R17,39
	BRLO _0x2060017
	CALL SUBOPT_0x30
	__POINTW2FN _0x2060000,5
	CALL _strcpyf
	RJMP _0x20C0008
_0x2060017:
	RJMP _0x2060014
_0x2060016:
	CPI  R17,0
	BRNE _0x2060018
	CALL SUBOPT_0x32
	LDI  R30,LOW(48)
	ST   X,R30
	RJMP _0x2060019
_0x2060018:
_0x206001A:
	MOV  R30,R17
	SUBI R17,1
	CPI  R30,0
	BREQ _0x206001C
	CALL SUBOPT_0x33
	CALL SUBOPT_0x34
	__GETD2N 0x3F000000
	CALL __ADDF12
	MOVW R26,R30
	MOVW R24,R22
	CALL _floor
	CALL SUBOPT_0x35
	CALL SUBOPT_0x36
	CALL __DIVF21
	CALL __CFD1U
	MOV  R16,R30
	CALL SUBOPT_0x32
	MOV  R30,R16
	SUBI R30,-LOW(48)
	ST   X,R30
	MOV  R30,R16
	LDI  R31,0
	CALL SUBOPT_0x33
	CALL SUBOPT_0x39
	CALL __MULF12
	CALL SUBOPT_0x3A
	CALL SUBOPT_0x25
	CALL SUBOPT_0x31
	RJMP _0x206001A
_0x206001C:
_0x2060019:
	LDD  R30,Y+8
	CPI  R30,0
	BREQ _0x20C0007
	CALL SUBOPT_0x32
	LDI  R30,LOW(46)
	ST   X,R30
_0x206001E:
	LDD  R30,Y+8
	SUBI R30,LOW(1)
	STD  Y+8,R30
	SUBI R30,-LOW(1)
	BREQ _0x2060020
	CALL SUBOPT_0x3A
	CALL SUBOPT_0x38
	CALL SUBOPT_0x31
	__GETD1S 9
	CALL __CFD1U
	MOV  R16,R30
	CALL SUBOPT_0x32
	MOV  R30,R16
	SUBI R30,-LOW(48)
	ST   X,R30
	MOV  R30,R16
	LDI  R31,0
	CALL SUBOPT_0x3A
	CALL SUBOPT_0x39
	CALL SUBOPT_0x25
	CALL SUBOPT_0x31
	RJMP _0x206001E
_0x2060020:
_0x20C0007:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LDI  R30,LOW(0)
	ST   X,R30
_0x20C0008:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,13
	RET
; .FEND

	.DSEG

	.CSEG

	.CSEG
_ftrunc:
; .FSTART _ftrunc
	CALL __PUTPARD2
   ldd  r23,y+3
   ldd  r22,y+2
   ldd  r31,y+1
   ld   r30,y
   bst  r23,7
   lsl  r23
   sbrc r22,7
   sbr  r23,1
   mov  r25,r23
   subi r25,0x7e
   breq __ftrunc0
   brcs __ftrunc0
   cpi  r25,24
   brsh __ftrunc1
   clr  r26
   clr  r27
   clr  r24
__ftrunc2:
   sec
   ror  r24
   ror  r27
   ror  r26
   dec  r25
   brne __ftrunc2
   and  r30,r26
   and  r31,r27
   and  r22,r24
   rjmp __ftrunc1
__ftrunc0:
   clt
   clr  r23
   clr  r30
   clr  r31
   clr  r22
__ftrunc1:
   cbr  r22,0x80
   lsr  r23
   brcc __ftrunc3
   sbr  r22,0x80
__ftrunc3:
   bld  r23,7
   ld   r26,y+
   ld   r27,y+
   ld   r24,y+
   ld   r25,y+
   cp   r30,r26
   cpc  r31,r27
   cpc  r22,r24
   cpc  r23,r25
   bst  r25,7
   ret
; .FEND
_floor:
; .FSTART _floor
	CALL SUBOPT_0x3B
	CALL _ftrunc
	CALL __PUTD1S0
    brne __floor1
__floor0:
	CALL SUBOPT_0x3C
	RJMP _0x20C0002
__floor1:
    brtc __floor0
	CALL SUBOPT_0x3C
	CALL SUBOPT_0x3D
	RJMP _0x20C0002
; .FEND
_log:
; .FSTART _log
	CALL __PUTPARD2
	SBIW R28,4
	ST   -Y,R17
	ST   -Y,R16
	CALL SUBOPT_0x3E
	CALL __CPD02
	BRLT _0x208000C
	__GETD1N 0xFF7FFFFF
	RJMP _0x20C0006
_0x208000C:
	CALL SUBOPT_0x3F
	CALL __PUTPARD1
	IN   R26,SPL
	IN   R27,SPH
	SBIW R26,1
	PUSH R17
	PUSH R16
	CALL _frexp
	POP  R16
	POP  R17
	CALL SUBOPT_0x40
	CALL SUBOPT_0x3E
	__GETD1N 0x3F3504F3
	CALL __CMPF12
	BRSH _0x208000D
	CALL SUBOPT_0x41
	CALL __ADDF12
	CALL SUBOPT_0x40
	__SUBWRN 16,17,1
_0x208000D:
	CALL SUBOPT_0x3F
	CALL SUBOPT_0x3D
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x3F
	__GETD2N 0x3F800000
	CALL __ADDF12
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __DIVF21
	CALL SUBOPT_0x40
	CALL SUBOPT_0x41
	CALL SUBOPT_0x42
	__GETD2N 0x3F654226
	CALL __MULF12
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x4054114E
	CALL SUBOPT_0x25
	CALL SUBOPT_0x3E
	CALL __MULF12
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x43
	__GETD2N 0x3FD4114D
	CALL __SUBF12
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __DIVF21
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	MOVW R30,R16
	CALL SUBOPT_0x39
	__GETD2N 0x3F317218
	CALL __MULF12
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __ADDF12
_0x20C0006:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,10
	RET
; .FEND
_exp:
; .FSTART _exp
	CALL __PUTPARD2
	SBIW R28,8
	ST   -Y,R17
	ST   -Y,R16
	CALL SUBOPT_0x44
	__GETD1N 0xC2AEAC50
	CALL __CMPF12
	BRSH _0x208000F
	CALL SUBOPT_0x45
	RJMP _0x20C0004
_0x208000F:
	__GETD1S 10
	CALL __CPD10
	BRNE _0x2080010
	CALL SUBOPT_0x37
	RJMP _0x20C0004
_0x2080010:
	CALL SUBOPT_0x44
	__GETD1N 0x42B17218
	CALL __CMPF12
	BREQ PC+2
	BRCC PC+2
	RJMP _0x2080011
	__GETD1N 0x7F7FFFFF
	RJMP _0x20C0004
_0x2080011:
	CALL SUBOPT_0x44
	__GETD1N 0x3FB8AA3B
	CALL __MULF12
	__PUTD1S 10
	CALL SUBOPT_0x44
	RCALL _floor
	CALL __CFD1
	MOVW R16,R30
	CALL SUBOPT_0x44
	CALL SUBOPT_0x39
	CALL SUBOPT_0x25
	MOVW R26,R30
	MOVW R24,R22
	CALL SUBOPT_0x46
	CALL SUBOPT_0x25
	CALL SUBOPT_0x40
	CALL SUBOPT_0x41
	CALL SUBOPT_0x42
	__GETD2N 0x3D6C4C6D
	CALL __MULF12
	__GETD2N 0x40E6E3A6
	CALL __ADDF12
	CALL SUBOPT_0x3E
	CALL __MULF12
	CALL SUBOPT_0x40
	CALL SUBOPT_0x43
	__GETD2N 0x41A68D28
	CALL __ADDF12
	CALL SUBOPT_0x35
	CALL SUBOPT_0x3F
	CALL SUBOPT_0x33
	CALL __ADDF12
	__GETD2N 0x3FB504F3
	CALL __MULF12
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0x3E
	CALL SUBOPT_0x43
	CALL __SUBF12
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __DIVF21
	CALL __PUTPARD1
	MOVW R26,R16
	CALL _ldexp
_0x20C0004:
	LDD  R17,Y+1
	LDD  R16,Y+0
_0x20C0005:
	ADIW R28,14
	RET
; .FEND
_pow:
; .FSTART _pow
	CALL __PUTPARD2
	SBIW R28,4
	CALL SUBOPT_0x47
	CALL __CPD10
	BRNE _0x2080012
	CALL SUBOPT_0x45
	RJMP _0x20C0003
_0x2080012:
	__GETD2S 8
	CALL __CPD02
	BRGE _0x2080013
	CALL SUBOPT_0x48
	CALL __CPD10
	BRNE _0x2080014
	CALL SUBOPT_0x37
	RJMP _0x20C0003
_0x2080014:
	__GETD2S 8
	CALL SUBOPT_0x49
	RJMP _0x20C0003
_0x2080013:
	CALL SUBOPT_0x48
	MOVW R26,R28
	CALL __CFD1
	CALL __PUTDP1
	CALL SUBOPT_0x3C
	CALL __CDF1
	MOVW R26,R30
	MOVW R24,R22
	CALL SUBOPT_0x48
	CALL __CPD12
	BREQ _0x2080015
	CALL SUBOPT_0x45
	RJMP _0x20C0003
_0x2080015:
	CALL SUBOPT_0x47
	CALL __ANEGF1
	MOVW R26,R30
	MOVW R24,R22
	CALL SUBOPT_0x49
	__PUTD1S 8
	LD   R30,Y
	ANDI R30,LOW(0x1)
	BRNE _0x2080016
	CALL SUBOPT_0x47
	RJMP _0x20C0003
_0x2080016:
	CALL SUBOPT_0x47
	CALL __ANEGF1
_0x20C0003:
	ADIW R28,12
	RET
; .FEND
_sin:
; .FSTART _sin
	CALL __PUTPARD2
	SBIW R28,4
	ST   -Y,R17
	LDI  R17,0
	CALL SUBOPT_0x4A
	__GETD1N 0x3E22F983
	CALL __MULF12
	CALL SUBOPT_0x4B
	RCALL _floor
	CALL SUBOPT_0x4A
	CALL SUBOPT_0x25
	CALL SUBOPT_0x4B
	CALL SUBOPT_0x46
	CALL __CMPF12
	BREQ PC+2
	BRCC PC+2
	RJMP _0x2080017
	CALL SUBOPT_0x4C
	__GETD2N 0x3F000000
	CALL SUBOPT_0x4D
	LDI  R17,LOW(1)
_0x2080017:
	CALL SUBOPT_0x4A
	__GETD1N 0x3E800000
	CALL __CMPF12
	BREQ PC+2
	BRCC PC+2
	RJMP _0x2080018
	CALL SUBOPT_0x4A
	CALL SUBOPT_0x46
	CALL SUBOPT_0x4D
_0x2080018:
	CPI  R17,0
	BREQ _0x2080019
	CALL SUBOPT_0x4C
	CALL __ANEGF1
	__PUTD1S 5
_0x2080019:
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x4A
	CALL __MULF12
	__PUTD1S 1
	CALL SUBOPT_0xD
	__GETD2N 0x4226C4B1
	CALL __MULF12
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x422DE51D
	CALL SUBOPT_0x25
	CALL SUBOPT_0xB
	CALL __MULF12
	__GETD2N 0x4104534C
	CALL __ADDF12
	CALL SUBOPT_0x4A
	CALL __MULF12
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	CALL SUBOPT_0xD
	__GETD2N 0x3FDEED11
	CALL __ADDF12
	CALL SUBOPT_0xB
	CALL __MULF12
	__GETD2N 0x3FA87B5E
	CALL __ADDF12
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	CALL __DIVF21
	LDD  R17,Y+0
	ADIW R28,9
	RET
; .FEND
_cos:
; .FSTART _cos
	CALL SUBOPT_0x3B
	__GETD1N 0x3FC90FDB
	CALL __SUBF12
	MOVW R26,R30
	MOVW R24,R22
	RCALL _sin
_0x20C0002:
	ADIW R28,4
	RET
; .FEND
_tan:
; .FSTART _tan
	CALL __PUTPARD2
	SBIW R28,4
	CALL SUBOPT_0x4E
	RCALL _cos
	CALL __PUTD1S0
	CALL __CPD10
	BRNE _0x208001A
	CALL SUBOPT_0x4E
	CALL __CPD02
	BRGE _0x208001B
	__GETD1N 0x7F7FFFFF
	RJMP _0x20C0001
_0x208001B:
	__GETD1N 0xFF7FFFFF
	RJMP _0x20C0001
_0x208001A:
	CALL SUBOPT_0x4E
	RCALL _sin
	MOVW R26,R30
	MOVW R24,R22
	CALL SUBOPT_0x3C
	CALL __DIVF21
_0x20C0001:
	ADIW R28,8
	RET
; .FEND

	.CSEG

	.DSEG
_msg:
	.BYTE 0x20
_expression:
	.BYTE 0x20
_val:
	.BYTE 0x80
_types:
	.BYTE 0x20
__base_y_G100:
	.BYTE 0x4
__lcd_maxx:
	.BYTE 0x1
__seed_G103:
	.BYTE 0x4

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x0:
	LDI  R30,LOW(_msg)
	LDI  R31,HIGH(_msg)
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:27 WORDS
SUBOPT_0x1:
	__POINTW1FN _0x0,0
	ST   -Y,R31
	ST   -Y,R30
	LDI  R24,0
	CALL _sprintf
	ADIW R28,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x2:
	LDI  R30,LOW(_expression)
	LDI  R31,HIGH(_expression)
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x3:
	__POINTW1FN _0x0,29
	ST   -Y,R31
	ST   -Y,R30
	LDI  R24,0
	CALL _sprintf
	ADIW R28,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 14 TIMES, CODE SIZE REDUCTION:36 WORDS
SUBOPT_0x4:
	MOVW R30,R16
	LDI  R26,LOW(_val)
	LDI  R27,HIGH(_val)
	CALL __LSLW2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x5:
	ADD  R26,R30
	ADC  R27,R31
	__GETD1N 0x0
	CALL __PUTDP1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 33 TIMES, CODE SIZE REDUCTION:61 WORDS
SUBOPT_0x6:
	LDI  R26,LOW(_types)
	LDI  R27,HIGH(_types)
	ADD  R26,R16
	ADC  R27,R17
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x7:
	LDI  R30,LOW(0)
	ST   Y,R30
	STD  Y+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x8:
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	CP   R30,R10
	CPC  R31,R11
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:33 WORDS
SUBOPT_0x9:
	MOVW R30,R8
	LDI  R26,LOW(_val)
	LDI  R27,HIGH(_val)
	CALL __LSLW2
	ADD  R26,R30
	ADC  R27,R31
	MOVW R30,R6
	CALL __CWD1
	CALL __CDF1
	CALL __PUTDP1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0xA:
	CLR  R10
	CLR  R11
	LDI  R26,LOW(_types)
	LDI  R27,HIGH(_types)
	ADD  R26,R8
	ADC  R27,R9
	LDI  R30,LOW(78)
	ST   X,R30
	MOVW R30,R8
	ADIW R30,1
	MOVW R8,R30
	CLR  R6
	CLR  R7
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xB:
	__GETD2S 1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xC:
	ST   -Y,R31
	ST   -Y,R30
	LDI  R24,0
	CALL _sprintf
	ADIW R28,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xD:
	__GETD1S 1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 16 TIMES, CODE SIZE REDUCTION:27 WORDS
SUBOPT_0xE:
	MOVW R30,R28
	ADIW R30,15
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 16 TIMES, CODE SIZE REDUCTION:87 WORDS
SUBOPT_0xF:
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(_expression)
	LDI  R31,HIGH(_expression)
	CLR  R22
	CLR  R23
	CALL __PUTPARD1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x10:
	CALL __PUTPARD1
	LDI  R24,8
	CALL _sprintf
	ADIW R28,12
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x11:
	MOVW R26,R28
	ADIW R26,17
	JMP  _strcpy

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x12:
	CLR  R4
	CLR  R5
	RJMP SUBOPT_0x0

;OPTIMIZER ADDED SUBROUTINE, CALLED 16 TIMES, CODE SIZE REDUCTION:27 WORDS
SUBOPT_0x13:
	LDI  R26,LOW(_types)
	LDI  R27,HIGH(_types)
	ADD  R26,R8
	ADC  R27,R9
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x14:
	LDI  R30,LOW(78)
	ST   X,R30
	MOVW R30,R8
	ADIW R30,1
	MOVW R8,R30
	CLR  R6
	CLR  R7
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 14 TIMES, CODE SIZE REDUCTION:49 WORDS
SUBOPT_0x15:
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
	RJMP SUBOPT_0x13

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:31 WORDS
SUBOPT_0x16:
	LDI  R26,LOW(_val)
	LDI  R27,HIGH(_val)
	CALL __LSLW2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:31 WORDS
SUBOPT_0x17:
	SUBI R30,LOW(-_types)
	SBCI R31,HIGH(-_types)
	LDI  R26,LOW(120)
	STD  Z+0,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x18:
	ST   X,R30
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	SBIW R30,2
	STD  Y+8,R30
	STD  Y+8+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:57 WORDS
SUBOPT_0x19:
	ST   -Y,R17
	ST   -Y,R16
	MOVW R26,R20
	CALL _get_next_num
	STD  Y+10,R30
	STD  Y+10+1,R31
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CPI  R26,LOW(0xFFFF)
	LDI  R30,HIGH(0xFFFF)
	CPC  R27,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x1A:
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	RJMP SUBOPT_0x16

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:31 WORDS
SUBOPT_0x1B:
	ADD  R26,R30
	ADC  R27,R31
	CALL __GETD1P
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:21 WORDS
SUBOPT_0x1C:
	CALL __PUTDP1
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	RJMP SUBOPT_0x17

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:27 WORDS
SUBOPT_0x1D:
	LDI  R30,LOW(78)
	ST   X,R30
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	SBIW R30,1
	STD  Y+8,R30
	STD  Y+8+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1E:
	MOVW R26,R30
	MOVW R24,R22
	JMP  _exp

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:57 WORDS
SUBOPT_0x1F:
	MOVW R26,R16
	CALL _get_last_num
	STD  Y+12,R30
	STD  Y+12+1,R31
	ST   -Y,R17
	ST   -Y,R16
	MOVW R26,R20
	CALL _get_next_num
	STD  Y+10,R30
	STD  Y+10+1,R31
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	CPI  R26,LOW(0xFFFF)
	LDI  R30,HIGH(0xFFFF)
	CPC  R27,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x20:
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CPI  R26,LOW(0xFFFF)
	LDI  R30,HIGH(0xFFFF)
	CPC  R27,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x21:
	LDD  R30,Y+12
	LDD  R31,Y+12+1
	RJMP SUBOPT_0x16

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x22:
	CALL __PUTDP1
	LDD  R30,Y+12
	LDD  R31,Y+12+1
	RJMP SUBOPT_0x17

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x23:
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	RJMP SUBOPT_0x17

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x24:
	__GETD1N 0x4CEB79A3
	CALL __LOADLOCR6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x25:
	CALL __SWAPD12
	CALL __SUBF12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x26:
	ST   -Y,R27
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
	__GETWRN 16,17,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x27:
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	LDD  R17,Y+1
	LDD  R16,Y+0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x28:
	CALL __lcd_write_data
	LDI  R26,LOW(3)
	LDI  R27,0
	JMP  _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x29:
	LDI  R26,LOW(48)
	CALL __lcd_write_nibble_G100
	__DELAY_USB 133
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x2A:
	ST   -Y,R18
	LDD  R26,Y+13
	LDD  R27,Y+13+1
	LDD  R30,Y+15
	LDD  R31,Y+15+1
	ICALL
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x2B:
	LDD  R30,Y+16
	LDD  R31,Y+16+1
	SBIW R30,4
	STD  Y+16,R30
	STD  Y+16+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x2C:
	LDD  R26,Y+13
	LDD  R27,Y+13+1
	LDD  R30,Y+15
	LDD  R31,Y+15+1
	ICALL
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x2D:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	ADIW R26,4
	CALL __GETW1P
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2E:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	ADIW R26,4
	CALL __GETW1P
	STD  Y+10,R30
	STD  Y+10+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x2F:
	MOVW R26,R28
	ADIW R26,12
	CALL __ADDW2R15
	CALL __GETW1P
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x30:
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x31:
	__PUTD1S 9
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x32:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	ADIW R26,1
	STD  Y+6,R26
	STD  Y+6+1,R27
	SBIW R26,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x33:
	__GETD2S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x34:
	__GETD1N 0x3DCCCCCD
	CALL __MULF12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x35:
	__PUTD1S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x36:
	__GETD1S 2
	__GETD2S 9
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x37:
	__GETD1N 0x3F800000
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x38:
	__GETD1N 0x41200000
	CALL __MULF12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x39:
	CALL __CWD1
	CALL __CDF1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3A:
	__GETD2S 9
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3B:
	CALL __PUTPARD2
	CALL __GETD2S0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x3C:
	CALL __GETD1S0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3D:
	__GETD2N 0x3F800000
	CALL __SUBF12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x3E:
	__GETD2S 6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x3F:
	__GETD1S 6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x40:
	__PUTD1S 6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x41:
	RCALL SUBOPT_0x3F
	RJMP SUBOPT_0x3E

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x42:
	CALL __MULF12
	RCALL SUBOPT_0x35
	__GETD1S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x43:
	__GETD1S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x44:
	__GETD2S 10
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x45:
	__GETD1N 0x0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x46:
	__GETD1N 0x3F000000
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x47:
	__GETD1S 8
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x48:
	__GETD1S 4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x49:
	CALL _log
	__GETD2S 4
	CALL __MULF12
	RJMP SUBOPT_0x1E

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x4A:
	__GETD2S 5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4B:
	__PUTD1S 5
	RJMP SUBOPT_0x4A

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4C:
	__GETD1S 5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4D:
	CALL __SUBF12
	__PUTD1S 5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4E:
	__GETD2S 4
	RET


	.CSEG
_delay_ms:
	adiw r26,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x3E8
	wdr
	sbiw r26,1
	brne __delay_ms0
__delay_ms1:
	ret

_frexp:
	LD   R30,Y+
	LD   R31,Y+
	LD   R22,Y+
	LD   R23,Y+
	BST  R23,7
	LSL  R22
	ROL  R23
	CLR  R24
	SUBI R23,0x7E
	SBC  R24,R24
	ST   X+,R23
	ST   X,R24
	LDI  R23,0x7E
	LSR  R23
	ROR  R22
	BRTS __ANEGF1
	RET

_ldexp:
	LD   R30,Y+
	LD   R31,Y+
	LD   R22,Y+
	LD   R23,Y+
	BST  R23,7
	LSL  R22
	ROL  R23
	ADD  R23,R26
	LSR  R23
	ROR  R22
	BRTS __ANEGF1
	RET

__ANEGF1:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	BREQ __ANEGF10
	SUBI R23,0x80
__ANEGF10:
	RET

__ROUND_REPACK:
	TST  R21
	BRPL __REPACK
	CPI  R21,0x80
	BRNE __ROUND_REPACK0
	SBRS R30,0
	RJMP __REPACK
__ROUND_REPACK0:
	ADIW R30,1
	ADC  R22,R25
	ADC  R23,R25
	BRVS __REPACK1

__REPACK:
	LDI  R21,0x80
	EOR  R21,R23
	BRNE __REPACK0
	PUSH R21
	RJMP __ZERORES
__REPACK0:
	CPI  R21,0xFF
	BREQ __REPACK1
	LSL  R22
	LSL  R0
	ROR  R21
	ROR  R22
	MOV  R23,R21
	RET
__REPACK1:
	PUSH R21
	TST  R0
	BRMI __REPACK2
	RJMP __MAXRES
__REPACK2:
	RJMP __MINRES

__UNPACK:
	LDI  R21,0x80
	MOV  R1,R25
	AND  R1,R21
	LSL  R24
	ROL  R25
	EOR  R25,R21
	LSL  R21
	ROR  R24

__UNPACK1:
	LDI  R21,0x80
	MOV  R0,R23
	AND  R0,R21
	LSL  R22
	ROL  R23
	EOR  R23,R21
	LSL  R21
	ROR  R22
	RET

__CFD1U:
	SET
	RJMP __CFD1U0
__CFD1:
	CLT
__CFD1U0:
	PUSH R21
	RCALL __UNPACK1
	CPI  R23,0x80
	BRLO __CFD10
	CPI  R23,0xFF
	BRCC __CFD10
	RJMP __ZERORES
__CFD10:
	LDI  R21,22
	SUB  R21,R23
	BRPL __CFD11
	NEG  R21
	CPI  R21,8
	BRTC __CFD19
	CPI  R21,9
__CFD19:
	BRLO __CFD17
	SER  R30
	SER  R31
	SER  R22
	LDI  R23,0x7F
	BLD  R23,7
	RJMP __CFD15
__CFD17:
	CLR  R23
	TST  R21
	BREQ __CFD15
__CFD18:
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R23
	DEC  R21
	BRNE __CFD18
	RJMP __CFD15
__CFD11:
	CLR  R23
__CFD12:
	CPI  R21,8
	BRLO __CFD13
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R23
	SUBI R21,8
	RJMP __CFD12
__CFD13:
	TST  R21
	BREQ __CFD15
__CFD14:
	LSR  R23
	ROR  R22
	ROR  R31
	ROR  R30
	DEC  R21
	BRNE __CFD14
__CFD15:
	TST  R0
	BRPL __CFD16
	RCALL __ANEGD1
__CFD16:
	POP  R21
	RET

__CDF1U:
	SET
	RJMP __CDF1U0
__CDF1:
	CLT
__CDF1U0:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	BREQ __CDF10
	CLR  R0
	BRTS __CDF11
	TST  R23
	BRPL __CDF11
	COM  R0
	RCALL __ANEGD1
__CDF11:
	MOV  R1,R23
	LDI  R23,30
	TST  R1
__CDF12:
	BRMI __CDF13
	DEC  R23
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R1
	RJMP __CDF12
__CDF13:
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R1
	PUSH R21
	RCALL __REPACK
	POP  R21
__CDF10:
	RET

__SWAPACC:
	PUSH R20
	MOVW R20,R30
	MOVW R30,R26
	MOVW R26,R20
	MOVW R20,R22
	MOVW R22,R24
	MOVW R24,R20
	MOV  R20,R0
	MOV  R0,R1
	MOV  R1,R20
	POP  R20
	RET

__UADD12:
	ADD  R30,R26
	ADC  R31,R27
	ADC  R22,R24
	RET

__NEGMAN1:
	COM  R30
	COM  R31
	COM  R22
	SUBI R30,-1
	SBCI R31,-1
	SBCI R22,-1
	RET

__SUBF12:
	PUSH R21
	RCALL __UNPACK
	CPI  R25,0x80
	BREQ __ADDF129
	LDI  R21,0x80
	EOR  R1,R21

	RJMP __ADDF120

__ADDF12:
	PUSH R21
	RCALL __UNPACK
	CPI  R25,0x80
	BREQ __ADDF129

__ADDF120:
	CPI  R23,0x80
	BREQ __ADDF128
__ADDF121:
	MOV  R21,R23
	SUB  R21,R25
	BRVS __ADDF1211
	BRPL __ADDF122
	RCALL __SWAPACC
	RJMP __ADDF121
__ADDF122:
	CPI  R21,24
	BRLO __ADDF123
	CLR  R26
	CLR  R27
	CLR  R24
__ADDF123:
	CPI  R21,8
	BRLO __ADDF124
	MOV  R26,R27
	MOV  R27,R24
	CLR  R24
	SUBI R21,8
	RJMP __ADDF123
__ADDF124:
	TST  R21
	BREQ __ADDF126
__ADDF125:
	LSR  R24
	ROR  R27
	ROR  R26
	DEC  R21
	BRNE __ADDF125
__ADDF126:
	MOV  R21,R0
	EOR  R21,R1
	BRMI __ADDF127
	RCALL __UADD12
	BRCC __ADDF129
	ROR  R22
	ROR  R31
	ROR  R30
	INC  R23
	BRVC __ADDF129
	RJMP __MAXRES
__ADDF128:
	RCALL __SWAPACC
__ADDF129:
	RCALL __REPACK
	POP  R21
	RET
__ADDF1211:
	BRCC __ADDF128
	RJMP __ADDF129
__ADDF127:
	SUB  R30,R26
	SBC  R31,R27
	SBC  R22,R24
	BREQ __ZERORES
	BRCC __ADDF1210
	COM  R0
	RCALL __NEGMAN1
__ADDF1210:
	TST  R22
	BRMI __ADDF129
	LSL  R30
	ROL  R31
	ROL  R22
	DEC  R23
	BRVC __ADDF1210

__ZERORES:
	CLR  R30
	CLR  R31
	CLR  R22
	CLR  R23
	POP  R21
	RET

__MINRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	SER  R23
	POP  R21
	RET

__MAXRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	LDI  R23,0x7F
	POP  R21
	RET

__MULF12:
	PUSH R21
	RCALL __UNPACK
	CPI  R23,0x80
	BREQ __ZERORES
	CPI  R25,0x80
	BREQ __ZERORES
	EOR  R0,R1
	SEC
	ADC  R23,R25
	BRVC __MULF124
	BRLT __ZERORES
__MULF125:
	TST  R0
	BRMI __MINRES
	RJMP __MAXRES
__MULF124:
	PUSH R0
	PUSH R17
	PUSH R18
	PUSH R19
	PUSH R20
	CLR  R17
	CLR  R18
	CLR  R25
	MUL  R22,R24
	MOVW R20,R0
	MUL  R24,R31
	MOV  R19,R0
	ADD  R20,R1
	ADC  R21,R25
	MUL  R22,R27
	ADD  R19,R0
	ADC  R20,R1
	ADC  R21,R25
	MUL  R24,R30
	RCALL __MULF126
	MUL  R27,R31
	RCALL __MULF126
	MUL  R22,R26
	RCALL __MULF126
	MUL  R27,R30
	RCALL __MULF127
	MUL  R26,R31
	RCALL __MULF127
	MUL  R26,R30
	ADD  R17,R1
	ADC  R18,R25
	ADC  R19,R25
	ADC  R20,R25
	ADC  R21,R25
	MOV  R30,R19
	MOV  R31,R20
	MOV  R22,R21
	MOV  R21,R18
	POP  R20
	POP  R19
	POP  R18
	POP  R17
	POP  R0
	TST  R22
	BRMI __MULF122
	LSL  R21
	ROL  R30
	ROL  R31
	ROL  R22
	RJMP __MULF123
__MULF122:
	INC  R23
	BRVS __MULF125
__MULF123:
	RCALL __ROUND_REPACK
	POP  R21
	RET

__MULF127:
	ADD  R17,R0
	ADC  R18,R1
	ADC  R19,R25
	RJMP __MULF128
__MULF126:
	ADD  R18,R0
	ADC  R19,R1
__MULF128:
	ADC  R20,R25
	ADC  R21,R25
	RET

__DIVF21:
	PUSH R21
	RCALL __UNPACK
	CPI  R23,0x80
	BRNE __DIVF210
	TST  R1
__DIVF211:
	BRPL __DIVF219
	RJMP __MINRES
__DIVF219:
	RJMP __MAXRES
__DIVF210:
	CPI  R25,0x80
	BRNE __DIVF218
__DIVF217:
	RJMP __ZERORES
__DIVF218:
	EOR  R0,R1
	SEC
	SBC  R25,R23
	BRVC __DIVF216
	BRLT __DIVF217
	TST  R0
	RJMP __DIVF211
__DIVF216:
	MOV  R23,R25
	PUSH R17
	PUSH R18
	PUSH R19
	PUSH R20
	CLR  R1
	CLR  R17
	CLR  R18
	CLR  R19
	CLR  R20
	CLR  R21
	LDI  R25,32
__DIVF212:
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	CPC  R20,R17
	BRLO __DIVF213
	SUB  R26,R30
	SBC  R27,R31
	SBC  R24,R22
	SBC  R20,R17
	SEC
	RJMP __DIVF214
__DIVF213:
	CLC
__DIVF214:
	ROL  R21
	ROL  R18
	ROL  R19
	ROL  R1
	ROL  R26
	ROL  R27
	ROL  R24
	ROL  R20
	DEC  R25
	BRNE __DIVF212
	MOVW R30,R18
	MOV  R22,R1
	POP  R20
	POP  R19
	POP  R18
	POP  R17
	TST  R22
	BRMI __DIVF215
	LSL  R21
	ROL  R30
	ROL  R31
	ROL  R22
	DEC  R23
	BRVS __DIVF217
__DIVF215:
	RCALL __ROUND_REPACK
	POP  R21
	RET

__CMPF12:
	TST  R25
	BRMI __CMPF120
	TST  R23
	BRMI __CMPF121
	CP   R25,R23
	BRLO __CMPF122
	BRNE __CMPF121
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	BRLO __CMPF122
	BREQ __CMPF123
__CMPF121:
	CLZ
	CLC
	RET
__CMPF122:
	CLZ
	SEC
	RET
__CMPF123:
	SEZ
	CLC
	RET
__CMPF120:
	TST  R23
	BRPL __CMPF122
	CP   R25,R23
	BRLO __CMPF121
	BRNE __CMPF122
	CP   R30,R26
	CPC  R31,R27
	CPC  R22,R24
	BRLO __CMPF122
	BREQ __CMPF123
	RJMP __CMPF121

_sqrt:
	rcall __PUTPARD2
	sbiw r28,4
	push r21
	ldd  r25,y+7
	tst  r25
	brne __sqrt0
	adiw r28,8
	rjmp __zerores
__sqrt0:
	brpl __sqrt1
	adiw r28,8
	rjmp __maxres
__sqrt1:
	push r20
	ldi  r20,66
	ldd  r24,y+6
	ldd  r27,y+5
	ldd  r26,y+4
__sqrt2:
	st   y,r24
	std  y+1,r25
	std  y+2,r26
	std  y+3,r27
	movw r30,r26
	movw r22,r24
	ldd  r26,y+4
	ldd  r27,y+5
	ldd  r24,y+6
	ldd  r25,y+7
	rcall __divf21
	ld   r24,y
	ldd  r25,y+1
	ldd  r26,y+2
	ldd  r27,y+3
	rcall __addf12
	rcall __unpack1
	dec  r23
	rcall __repack
	ld   r24,y
	ldd  r25,y+1
	ldd  r26,y+2
	ldd  r27,y+3
	eor  r26,r30
	andi r26,0xf8
	brne __sqrt4
	cp   r27,r31
	cpc  r24,r22
	cpc  r25,r23
	breq __sqrt3
__sqrt4:
	dec  r20
	breq __sqrt3
	movw r26,r30
	movw r24,r22
	rjmp __sqrt2
__sqrt3:
	pop  r20
	pop  r21
	adiw r28,8
	ret

__ADDW2R15:
	CLR  R0
	ADD  R26,R15
	ADC  R27,R0
	RET

__ANEGW1:
	NEG  R31
	NEG  R30
	SBCI R31,0
	RET

__ANEGD1:
	COM  R31
	COM  R22
	COM  R23
	NEG  R30
	SBCI R31,-1
	SBCI R22,-1
	SBCI R23,-1
	RET

__LSLW2:
	LSL  R30
	ROL  R31
	LSL  R30
	ROL  R31
	RET

__CWD1:
	MOV  R22,R31
	ADD  R22,R22
	SBC  R22,R22
	MOV  R23,R22
	RET

__MULW12U:
	MUL  R31,R26
	MOV  R31,R0
	MUL  R30,R27
	ADD  R31,R0
	MUL  R30,R26
	MOV  R30,R0
	ADD  R31,R1
	RET

__MULW12:
	RCALL __CHKSIGNW
	RCALL __MULW12U
	BRTC __MULW121
	RCALL __ANEGW1
__MULW121:
	RET

__CHKSIGNW:
	CLT
	SBRS R31,7
	RJMP __CHKSW1
	RCALL __ANEGW1
	SET
__CHKSW1:
	SBRS R27,7
	RJMP __CHKSW2
	COM  R26
	COM  R27
	ADIW R26,1
	BLD  R0,0
	INC  R0
	BST  R0,0
__CHKSW2:
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	SBIW R26,1
	RET

__GETD1P:
	LD   R30,X+
	LD   R31,X+
	LD   R22,X+
	LD   R23,X
	SBIW R26,3
	RET

__PUTDP1:
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	RET

__GETW1PF:
	LPM  R0,Z+
	LPM  R31,Z
	MOV  R30,R0
	RET

__GETD1S0:
	LD   R30,Y
	LDD  R31,Y+1
	LDD  R22,Y+2
	LDD  R23,Y+3
	RET

__GETD2S0:
	LD   R26,Y
	LDD  R27,Y+1
	LDD  R24,Y+2
	LDD  R25,Y+3
	RET

__PUTD1S0:
	ST   Y,R30
	STD  Y+1,R31
	STD  Y+2,R22
	STD  Y+3,R23
	RET

__PUTPARD1:
	ST   -Y,R23
	ST   -Y,R22
	ST   -Y,R31
	ST   -Y,R30
	RET

__PUTPARD2:
	ST   -Y,R25
	ST   -Y,R24
	ST   -Y,R27
	ST   -Y,R26
	RET

__SWAPD12:
	MOV  R1,R24
	MOV  R24,R22
	MOV  R22,R1
	MOV  R1,R25
	MOV  R25,R23
	MOV  R23,R1

__SWAPW12:
	MOV  R1,R27
	MOV  R27,R31
	MOV  R31,R1

__SWAPB12:
	MOV  R1,R26
	MOV  R26,R30
	MOV  R30,R1
	RET

__CPD10:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	RET

__CPD02:
	CLR  R0
	CP   R0,R26
	CPC  R0,R27
	CPC  R0,R24
	CPC  R0,R25
	RET

__CPD12:
	CP   R30,R26
	CPC  R31,R27
	CPC  R22,R24
	CPC  R23,R25
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
