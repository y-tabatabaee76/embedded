#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
byte f1 = 0, f2 = 0, v1 = 0, v2 = 0, v3 = 0;

void setup() {
  lcd.begin(16, 2);
  Serial.begin(9600);
}

void loop() {
  lcd.clear();
  float voltage;
  
  if (Serial.available() > 0) {
    f1 = Serial.read();
    f2 = Serial.read();
    v1 = Serial.read();
    v2 = Serial.read();
    v3 = Serial.read();
  }
    
  lcd.setCursor(0, 0);
  lcd.print("freq :");
  lcd.print(f1);
  lcd.print(f2);
  delay(100);
  lcd.setCursor(0, 1);
  lcd.print("voltage :");
  lcd.print(v1);
  lcd.print(".");
  lcd.print(v2);
  lcd.print(v3);
  
  delay(1000);

}