const int led_pin = 9;
const int poten_pin = A0;

int analog_in = 0;
int frequency = 0;
float voltage = 0;

void setup () {
  pinMode(led_pin, OUTPUT);
  pinMode(poten_pin, INPUT);
  Serial.begin(9600);
}

void loop () {
  analog_in = analogRead(poten_pin);
  frequency = map(analog_in, 0, 1023, 0, 20);
  fade_led(frequency);
  voltage = (float) frequency / 4;
  
  byte f1 = frequency / 10;
  byte f2 = frequency % 10;
  byte v1 = (int) voltage;
  byte v2 = (int) (voltage * 10) % 10;
  byte v3 = (int) (voltage * 100) % 10;
  
  Serial.write(f1);
  Serial.write(f2);
  Serial.write(v1);
  Serial.write(v2);
  Serial.write(v3);
  
  // wait 1 second before next read
  delay(1000);
}

// fading led with given frequency
void fade_led (int frequency) {
  for (int val = 0; val < 255; val += frequency) {
    analogWrite(led_pin, val);
    delay(5);
  }
  for (int val = 255; val > 0; val -= frequency) {
    analogWrite(led_pin, val);
    delay(5);
  }  
}